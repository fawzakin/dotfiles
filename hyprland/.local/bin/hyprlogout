#!/usr/bin/env bash
shopt -s extglob

LOCKING="$HOME/Pictures/Locker"
LOCKER="$HOME/.config/hypr/locker.png"

image() {
    find $LOCKING -type f \( -iname "*.jpg" -o -iname "*.png" -o -iname "*.jpeg" -o -iname "*.gif" \) | shuf -n 1
}

locker() {
    [ ! -x "$(command -v hyprlock)" ] && notify-send "Hyprlock is not available" && exit 1
    IMG="$(image)"
    [ -f "$IMG" ] && cp -f "$IMG" "$LOCKER"
    hyprctl dispatch dpms on eDP-1
    loginctl lock-session
    pidof hyprlock || hyprlock
}

reload() {
    hyprctl reload 
    killall waybar
    (killall hyprpaper && sleep 1 && hyprpaper &) &
    notify-send "Configuration Reloaded!" "Hyprland, Hyprpaper, Waybar"
}

logout() {
    kill -9 $(cat ~/.cache/waypid)
    hyprctl dispatch exit
}

help_msg() {
    echo "USAGE: hyprlocker [--reload | --image | --locker]"
    echo "To get selection of logout menu, don't put any options!"
}

case "$1" in
    --help|-h) help_msg && exit 0;;
    --reload|-r) reload && exit 0;;
    --image|-i) image && exit 0;;
    --locker|-l) locker && exit 0;;
esac

logopt="$(printf \
' Reload
 Lock
󰒲 Suspend
 Logout
 Reboot
 Shutdown' \
| rofi -dmenu -l 6 -p "Options:")"

if   [[ "$logopt" =~ .*Reload$ ]]   ; then reload
elif [[ "$logopt" =~ .*Suspend$ ]]  ; then systemctl suspend
elif [[ "$logopt" =~ .*Lock$ ]]     ; then locker
elif [[ "$logopt" =~ .*Logout$ ]]   ; then logout
elif [[ "$logopt" =~ .*Reboot$ ]]   ; then systemctl reboot
elif [[ "$logopt" =~ .*Shutdown$ ]] ; then systemctl poweroff
else exit 1
fi
