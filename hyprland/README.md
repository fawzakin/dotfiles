# DWM config
This directory is the place for everything needed for the legacy dwm setup. It's been superseded by Hyprland but I may continue to use dwm in case there's an important application whose feature requires xorg or doesn't work on Wayland yet.

![dwm](dwm.png)

