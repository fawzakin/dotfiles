#=== Programs ===#
# Main Apps
$terminal = footclient
$browser = firefox
$file = thunar
$menu = rofi -show combi -modes combi -combi-modes "drun,run" -show-icons

# scripts
$monitor = hyprmonitor
$logout = hyprlogout
$sp = hyprspecial
$sc = screenshot
$rofi = rofi-script
$pick = hyprpicker-notify
$kill = notify-send "Killing Mode Activated!" "Press ESC to cancel"; hyprctl kill
$reway = killall waybar

# Special
$music = footclient -a musicterm -e ncmpcpp
$calc = footclient -a calcterm -t calculator -e qalc -i

# Multimedia
$pp = playerctl play-pause || mpc toggle
$st = playerctl stop || mpc stop
$pr = playerctl previous || mpc previous
$nx = playerctl next || mpc next

# Mod keys
$M = SUPER 
$S = SHIFT
$C = CONTROL_L
$A = ALT_L

#=== Launch Keybinding ===#
# Main Apps
bind = $M, M, exec, $file
bind = $M, B, exec, $browser
bind = $M, Return, exec, $terminal
bind = $M, Slash, exec, $menu
bind = $M, F1, exec, $reway

# Scripts
bind = $M, 8, exec, $rofi --radio
bind = $M $S, 8, exec, mpc-like
bind = $M, 9, exec, $monitor --toggle
bind = $M, 0, exec, $monitor
bind = $M $S, 0, exec, $rofi --wall
bind = $M, Minus, exec, $rofi --config
bind = $M, Equal, exec, $rofi --copy
bind = $M, Backspace, exec, $rofi --web
bind = $M $S, Escape, exec, $logout --reload
bindl = $M, Escape, exec, $logout
bindl = , XF86PowerOff, exec, $logout
bind = $M, Pause, exec, $pick
bind = $M, Insert, exec, $pick
bind = $M, Delete, exec, $kill

# Screenshots
bind = , Print, exec, $sc --focus-monitor
bind = $C, Print, exec, $sc --area
bind = $S, Print, exec, $sc --window
bind = $C $S, Print, exec, $sc --all-monitor
bind = $M, Print, exec, $sc --open

# Special Workspace (A.K.A. Scratchpad)
bind = $M, N, exec, $sp music "$music"
bind = , XF86Calculator, exec, $sp calc "$calc"

#=== Functionality ===#
# Basic control
bind = $M, F, togglefloating,
bind = $M, G, fullscreen,
bind = $M, Grave, killactive,
bind = $M, Tab, workspace, previous
bind = $A, Tab, workspace, previous

# Move focus
bind = $M, H, movefocus, l
bind = $M, L, movefocus, r
bind = $M, K, movefocus, u
bind = $M, J, movefocus, d

# Move a window
bind = $M $S, H, movewindow, l
bind = $M $S, L, movewindow, r
bind = $M $S, K, movewindow, u
bind = $M $S, J, movewindow, d

# Resize a window
bind = $M $C, H, resizeactive, -80 0
bind = $M $C, L, resizeactive, 80 0
bind = $M $C, K, resizeactive, 0 -40
bind = $M $C, J, resizeactive, 0 40

# Switch workspaces 
bind = $M, 1, workspace, 1
bind = $M, 2, workspace, 2
bind = $M, 3, workspace, 3
bind = $M, Q, workspace, 4
bind = $M, W, workspace, 5
bind = $M, E, workspace, 6
bind = $M, A, workspace, 7
bind = $M, S, workspace, 8
bind = $M, D, workspace, 9

# Move active window to a workspace
bind = $M $S, 1, movetoworkspacesilent, 1
bind = $M $S, 2, movetoworkspacesilent, 2
bind = $M $S, 3, movetoworkspacesilent, 3
bind = $M $S, Q, movetoworkspacesilent, 4
bind = $M $S, W, movetoworkspacesilent, 5
bind = $M $S, E, movetoworkspacesilent, 6
bind = $M $S, A, movetoworkspacesilent, 7
bind = $M $S, S, movetoworkspacesilent, 8
bind = $M $S, D, movetoworkspacesilent, 9

# Scroll through existing workspaces with mainMod + scroll
bind = $M, mouse_down, workspace, e+1
bind = $M, mouse_up, workspace, e-1

# Monitor Hellresizewindow
bind = $M, Period, focusmonitor, r
bind = $M, Comma, focusmonitor, l
bind = $M, Apostrophe, movewindow, mon:r
bind = $M, Semicolon, movewindow, mon:l
bind = $M $S, Period, movewindow, mon:r silent
bind = $M $S, Comma, movewindow, mon:l silent
bind = $M $C, Period, movecurrentworkspacetomonitor, r
bind = $M $C, Comma, movecurrentworkspacetomonitor, l

# Move/resize windows with mainMod + LMB/RMB and dragging
bindm = $M, mouse:272, movewindow
bindm = $M, mouse:273, resizewindow
bind = $M, mouse:274, movewindow, mon:+1 silent
bind = , mouse:277, cyclenext
#bind = $M, mouse:274, movewindow, mon:+1 silent

#=== Media Keys ===#
bindel = , XF86MonBrightnessUp, exec, brightnessctl set 10%+
bindel = , XF86MonBrightnessDown, exec, brightnessctl set 10%-
bindel = , XF86AudioRaiseVolume, exec, pamixer -i 5
bindel = , XF86AudioLowerVolume, exec, pamixer -d 5
bindl = , XF86AudioMute, exec, pamixer -t
bindl = , XF86AudioPlay, exec, $pp
bindl = , XF86AudioStop, exec, $st
bindl = , XF86AudioPrev, exec, $pr
bindl = , XF86AudioNext, exec, $nx

# For my laptop that doesn't come with multimedia key and I only notice it AFTER I bought it :/
bindl = $M, 4, exec, $pp
bindl = $M, 5, exec, $st
bindl = $M, 6, exec, $pr
bindl = $M, 7, exec, $nx
