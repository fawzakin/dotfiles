#!/usr/bin/env bash
# Stow every package and report any conflict to ~/.stow-conflict
for dir in */; do stow --no-folding -t ~ "$dir" 2> /dev/null || echo "$dir" >> ~/.stow-conflict; done
