#!/bin/sh -e
# Script by AN3223. Modified by fawzakin for saving changes to cache dir when autentication failed
# https://github.com/AN3223/scripts

help() {
	cat - >&2 <<EOF
doasedit - like sudoedit, but for doas

doasedit file...

Every argument will be treated as a file to edit. There's no support for
passing arguments to doas, so you can only doas root.

This script is SECURITY SENSITIVE! Special care has been taken to correctly
preserve file attributes. Please exercise CAUTION when modifying AND using
this script.
EOF
}

case "$1" in
	--help|-h) help; exit 0;;
	*) ;;
esac

export TMPDIR=/dev/shm/
trap 'rm -f "$tmp" "$tmpcopy"' EXIT HUP QUIT TERM INT ABRT

for file; do
    case "$file" in -*) file=./"$file" ;; esac

    tmp="$(mktemp)"
    if [ -f "$file" ] && [ ! -r "$file" ]; then
        doas cat "$file" > "$tmp"
    elif [ -r "$file" ]; then
        cat "$file" > "$tmp"
    fi

    tmpcopy="$(mktemp)"
    cat "$tmp" > "$tmpcopy"

    export TMPDIR=~/.cache
    tmpfail="$(mktemp)"

    ${EDITOR:-vi} "$tmp"

    if cmp -s "$tmp" "$tmpcopy"; then
        echo 'File unchanged, exiting...'
    else
        cp $tmp $tmpfail
        if doas dd if="$tmp" of="$file"; then
            echo 'Done, changes written'
            rm $tmpfail
        else
            echo "Current edit is saved as $tmpfail"
            exit 1
        fi
    fi

    rm "$tmp" "$tmpcopy" 2>/dev/null
done
