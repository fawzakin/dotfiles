# Useful scripts
The following are the brief documentation of each scripts. [D] means that dekstop file is available.

### colorterm
Print the color value of the scheme of my terminal configs, based from the tango colorscheme.

### colortest
Print 256 color test pattern. Created by [Tom Hale](https://gist.github.com/HaleTom/89ffe32783f89f403bba96bd7bcd1263).

### colortrue
Print true color spectrum. Taken from [here](https://unix.stackexchange.com/questions/404414/print-true-color-24-bit-test-pattern).

### compress-png
Compress multiple png files.

### doasedit
Like sudoedit but for doas. Created by [ANS3223](https://github.com/AN3223/scripts) and modified by me to save changes to cache dir when autentication failed.

### godot-nvim [D]
Wrapper to use rofi to launch Godot project and neovim (through neovide).  
Make sure to enable "external editor" in the setting and input the following configurations:  
- `exec path`: nvim
- `exec flag`: --server /tmp/godot.pipe --remote-send "<esc>:n {file}<CR>"

It will assume the location `~/Documents/Godot` for all of your Godot project on the latest version. You can pass `-4.2` and `-3` flag to use `~/Document/Godot4.2` and `~/Document/Godot3` instead.

### mpc-like
If you are using mpd, running this will append the currently playing song title and artist to `~/Music/liked_music`. Very useful if you are playing a radio and want to save the currently playing music as a "favorite".

### nsxiv-url
If you have nsxiv, this would allow you to open an image from an URL.

### nvim-st [D]
Run nvim on st.

### pdf
Compile a latex file and show the output using Zathura. You can press enter to recompile latex and reload the PDF output.

### playerctl-like
Like `mpd-like`, but uses playerctl instead of mpd. It will share the same `~/Music/liked_music` file.

### ratio
Enter two numbers to see their ratio. You can add third number to calculate its pair in accordance to the ratio the script just calculated.

Requires bc or qalc.

### swayimg-url
If you have swayimg, this would allow you to open an image from an URL.

# Convert
This script collection allows you to convert files to multiple format. Currently, it only support conversion for audio, image, and video.

### convert
Convert audio, image, or video to a different file format.

### transform
Gives control on how the input is transform using yad.
