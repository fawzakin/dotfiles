#!/usr/bin/env sh
# The autostart script made specifically for dwm. Other wm may work.

# ===| Resolution for VMs |===
# Uncomment one of the following to enable
#xrandr -s 1920x1080
#xrandr -s 1366x768
#xrandr -s 1280x720

# ===| Multimon Setup |===
if xrandr | grep -Eq 'HDMI[^ ]* connected'; then
    monitor-select -e
else
    monitor-select -r
    xwallpaper --maximize "${WALLPAPER:=$HOME/.local/backgrounds/dracula.png}"
fi

# ===| Startup Programs |===
startup="${COMP:=picom} lxpolkit dunst flameshot nm-applet mpd mpDris2"

for progs in $startup; do
	pidof -s "$progs" || setsid -f "$progs"
done >/dev/null 2>&1

# Redshift requires special treatment
{ pidof -s redshift || setsid -f redshift-gtk; } >/dev/null 2>&1

# ===| dwm |===
if pidof -s dwm > /dev/null; then
    for pidofbar in $(ps --user "$USER" -F | grep dwmstatusbar | awk '{print $2}'); do kill -15 "${pidofbar}" 2> /dev/null; done
    { dwmstatusbar || xsetroot -name " Statusbar not running!"; } &
    [ -e "$HOME/.cache/dwmcs" ] && rm -f "$HOME/.cache/dwmcs" # Remove cheatsheet cache
fi

# ===| Natural Scrolling |===
xinput set-prop "ELAN0634:00 04F3:3124 Touchpad" "libinput Natural Scrolling Enabled" 1 # Main Lenowo Laptop

# ===| Keybinding |===
# Set caps lock to right super key as mod3 since it is a useless key anyway
# Install xcape to send escape key from caps lock when releasing it under few milliseconds
xmodmap ${XDG_CONFIG_HOME:-$HOME/.config}/x11/xmodmap  
xcape -t 150 -e 'Super_R=Escape'

/usr/local/bin/thunar --daemon

# ===| Update Friday |===
# If you are on Arch and has pacman-contrib installed, check for update every friday.
# Make sure to put this on the last line
command -v checkupdates && [ "$(date +"%a")" = "Fri" ] && upnum=$(checkupdates | wc -l) && [ "$upnum" -ge 5 ] &&
notify-send -u normal "Update Friday" "$upnum packages to be updated."
