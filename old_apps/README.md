# Old Apps (Don't Use)
This directory is the place for everything needed for the legacy dwm setup as well as old apps I no longer use. It's been superseded by Hyprland and the new dwm build which I continue to use.

Please don't stow or use this. Always use the newest dwm build!

![dwm_old](dwm_old.png)

