# Rofi 
This package contains custom scripts that uses rofi as its main interface as well as rofi configuration itself. [D] means that dekstop file is available.

### makem3u
A python script to generate .m3u file from music stored in ~/Music for mpd. Will be sorted based of title or album track.

Note that this script currently only detects mp3, flac, and m4a at the moment. All other file types are ignored.

### makem3u-rofi [D]
Run makem3u in rofi

### mlp-watch [D]
Watch My Little Pony: Friendship Is Magic using rofi. It uses either [heartshine.gay](https://fim.heartshine.gay/) or [ponylove.cc](https://ponylove.cc/#cat-20) as the server and stream the episode using mpv (or whatever player set with `-v` flag. Only VLC is tested). 

### rofi-script
- `conf`: Edit configuration files (or any file editable using a terminal editor).
- `copy`: Copy pre-defined values from the config.
- `web`: Search the web from sites listed in config.
- `wall`: Set the wallpaper. See the [Wallpaper](#wallpaper) section below for detailed information.
- `radio`: Play radio either from a list in the config or from a playlist.

The configuration can be found on `~/.config/rofi-script`

#### Wallpaper
You need to create a folder at `~/Pictures/Wallpaper`, put your wallpaper there, and set your `WALLPAPER` environment variable to `$HOME/Pictures/Wallpaper/main.png` in order to set your wallpaper next time you login. The script will create a soft link of the selected wallpaper to `apply/main.png`.

The script can detect the current session. By default, if you are on Xorg, it will use `xwallpaper`. Likewise, if you are on Wayland, it will use `swww`. 

You can define the location of the wallpaper and the program to set the wallpaper with in the `wall` config.
