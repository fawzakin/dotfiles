#!/usr/bin/env bash
# TODO: Add support for movie, eg, and other stuff
# TODO: G5 should be on its own script

# === CONFIGURATION === #
OPTIND=1
MENU="rofi -dmenu -i -l 13 -p"
LIST="$HOME/.local/share/mlp_list.txt"
CACHE="$HOME/.cache/mlp-watch"
PLAYER="${VIDEO:-mpv}"

# === DO NOT EDIT THIS === #
SE="00" # Season
EP="00" # Episode
SRC=""  # Source
VID=""   # URL
SUB=""   # Sub
TITLE="" # Title of the episode

# === OPTARG options ===
SKIP_SUB=0

declare -A sources
#sources["Source Name"]="sub-key https://example.com/s%se%s.webm"
sources["Heartshine 720p"]="hs https://static.heartshine.gay/g4-fim/s%se%s-720p.mp4"
sources["Heartshine 1080p"]="hs https://static.heartshine.gay/g4-fim/s%se%s-1080p.mp4"
sources["Ponylove"]="pl pl"

declare -A subs
#sources["key"]="https://example.com/s%se%s.vtt"
subs["hs"]="https://static.heartshine.gay/g4-fim/vtt/s%se%s-en.vtt"
subs["pl"]="https://ponylove.cc/subtitles/YP-1A-%sx%s_av1.webm.vtt"
subs["pr"]="https://ponylove.cc/subtitles/YP-1R-%sx%s_av1.webm.vtt"

declare -A ponylove
ponylove["1"]="https://mrx.xxnightmaremoonxx.de/equestria/yp-1r-%sx%s_av1.webm"
ponylove["2"]="https://ponylove.rainbowdash.cc/YP-1A-%sx%s_av1.webm"
ponylove["3"]="https://mrx.xxnightmaremoonxx.de/equestria/YP-1R-%sx%s-FIX_av1.webm"

error() { echo "$1"; return 1; }
error_notify() { notify-send "$1" "$2"; exit 1; }
get_url() { printf "$1" "$SE" "$EP"; }
get_help() {
    echo "USAGE: $(basename -- $0) [OPTIONS]"
    echo "OPTIONS:"
    echo "  -v [PLAYER]"
    echo "      Use other video player."
    echo "      By default, this script will use the 'VIDEO' environment variable."
    echo "      If 'VIDEO' is not set, mpv will be used."
    echo "  -s: Skip subtitle from being retrieved."
    echo "  -h: Show this message."
    exit $1
}

get_episode() {
    MSG="Select Episode"
    TITLE="$(sed -e '/^$/d' -e '/^#.*/d' "${LIST}" | ${MENU} "${MSG}")" || exit 0
    SE="0${TITLE:1:1}"
    EP="${TITLE:3:2}"
}

get_source() {
    MSG="Select Source"
    SRC="$(printf '%s\n' "${!sources[@]}" | sort -f | ${MENU} "${MSG}")" || exit 0
    notify-send "Playing Episode from $SRC" "$TITLE" 

    TMP="${sources["$SRC"]}"
    SUBTMP="$(echo $TMP | cut -d" " -f1)"
    VIDTMP="$(echo $TMP | cut -d" " -f2)"

    get_sub "$SUBTMP"
    get_vid "$VIDTMP"
    get_vid_pl "$VIDTMP"
}

get_sub() {
    [ ! -d "$CACHE" ] && [ ! -z "$CACHE" ] && rm -rf "$CACHE" && mkdir -p "$CACHE"
    [ "$SKIP_SUB" -eq 1 ] && return 0
    SUBURL="$(get_url "${subs["$1"]}")"
    SUB="${CACHE}/s${SE}e${EP}-${1}.vtt"
    [ ! -f "$SUB" ] && curl $SUBURL --output "$SUB"
    if [ "$1" = "pl" ] && ! grep -q "WEBVTT" $SUB; then
        SUBURL="$(get_url "${subs["pr"]}")"
        curl $SUBURL --output "$SUB" || { notify-send "Cannot get subtitle" "Continuing"; SUB=""; }
    fi
}

get_vid() {
    [ "$1" = "pl" ] && return 0
    VID="$(get_url "$1")"
}

get_vid_pl() {
    [ ! "$1" = "pl" ] && return 0
    [ ! -d "$CACHE" ] && [ ! -z "$CACHE" ] && rm -rf "$CACHE" && mkdir -p "$CACHE"
    PLURL="${CACHE}/s${SE}e${EP}-${1}.txt"
    if [ ! -f "$PLURL" ]; then
        for U in "${ponylove[@]}"; do
            VID="$(get_url "$U")"
            PLSTAT="$(curl --silent --head "$VID" | awk '/^HTTP/{print $2}')"
            [ ! "$PLSTAT" = "404" ] && echo "$VID" > "$PLURL" && return 0
        done
        error_notify "Cannot get $TITLE from Ponylove" "Episode not exists or website error"
    else
        VID="$(cat $PLURL)"
    fi
}

play_video() {
    echo "Subtitle: $SUB"
    echo "Video URL: $VID"
    if [ ! -f "$SUB" ] || [ "$SKIP_SUB" -eq 1 ]; then
        $PLAYER "$VID" || error_notify "An error has occured" "Can't load URL or app issue"
    else
        $PLAYER --sub-file="$SUB" "$VID" || error_notify "An error has occured" "Can't load URL or app issue"
    fi
}
# === Options === #
while getopts "v:sh" opt; do case "$opt" in
    v) PLAYER="$OPTARG" ;;
    s) SKIP_SUB=1 ;;
    h) get_help 0 ;;
esac done

get_episode
get_source
play_video
