# NOTE: All lines that start with # will be ignored!
#=====[SEASON 1]=====
S1E01 Friendship is Magic, part 1
S1E02 Friendship is Magic, part 2
S1E03 The Ticket Master
S1E04 Applebuck Season
S1E05 Griffon the Brush Off
S1E06 Boast Busters
S1E07 Dragonshy
S1E08 Look Before You Sleep
S1E09 Bridle Gossip
S1E10 Swarm of the Century
S1E11 Winter Wrap Up
S1E12 Call of the Cutie
S1E13 Fall Weather Friends
S1E14 Suited for Success
S1E15 Feeling Pinkie Keen
S1E16 Sonic Rainboom
S1E17 Stare Master
S1E18 The Show Stoppers
S1E19 A Dog and Pony Show
S1E20 Green Isn't Your Color
S1E21 Over a Barrel
S1E22 A Bird in the Hoof
S1E23 Cutie Mark Chronicles
S1E24 Owl's Well That Ends Well
S1E25 Party of One
S1E26 Best Night Ever

#=====[SEASON 2]=====
S2E01 Return of Harmony, part 1
S2E02 Return of Harmony, part 2
S2E03 Lesson Zero
S2E04 Luna Eclipsed
S2E05 Sisterhooves Social
S2E06 The Cutie Pox
S2E07 May the Best Pet Win
S2E08 The Mysterious Mare Do Well
S2E09 Sweet and Elite
S2E10 Secret of my Excess
S2E11 Hearth's Warming Eve
S2E12 Family Appreciation Day
S2E13 Baby Cakes
S2E14 The Last Roundup
S2E15 The Super Speedy Cider Squeezy 6000
S2E16 Read It and Weep
S2E17 Hearts and Hooves Day
S2E18 A Friend in Deed
S2E19 Putting Your Hoof Down
S2E20 It's About Time
S2E21 Dragon Quest
S2E22 Hurricane Fluttershy
S2E23 Ponyville Confidential
S2E24 MMMystery on the Friendship Express
S2E25 A Canterlot Wedding, part 1
S2E26 A Canterlot Wedding, part 2

#=====[SEASON 3]=====
S3E01 The Crystal Empire, part 1
S3E02 The Crystal Empire, part 2
S3E03 Too Many Pinkie Pies
S3E04 One Bad Apple
S3E05 Magic Duel
S3E06 Sleepless in Ponyville
S3E07 Wonderbolt Academy
S3E08 Apple Family Reunio
S3E09 Spike at Your Service
S3E10 Keep Calm and Flutter On
S3E11 Just for Sidekicks
S3E12 Games Ponies Play
S3E13 Magical Mystery Cure

#=====[SEASON 4]=====
S4E01 Princess Twilight Sparkle, part 1
S4E02 Princess Twilight Sparkle, part 2
S4E03 Castle Mane-ia
S4E04 Daring Don't
S4E05 Flight to the Finish
S4E06 Power Ponies
S4E07 Bats!
S4E08 Rarity Takes Manehattan
S4E09 Pinkie Apple Pie
S4E10 Rainbow Falls
S4E11 Three's a Crowd
S4E12 Pinkie Pride
S4E13 Simple Ways
S4E14 Filli Vanilli
S4E15 Twilight Time
S4E16 It Ain't Easy Being Breezies
S4E17 Somepony to Watch Over Me
S4E18 Maud Pie
S4E19 For Whom the Sweetie Belle Toils
S4E20 Leap of Faith
S4E21 Testing Testing, 1, 2, 3
S4E22 Trade Ya
S4E23 Inspiration Manifestation
S4E24 Equestria Games
S4E25 Twilight's Kingdom, part 1
S4E26 Twilight's Kingdom, part 2

#=====[SEASON 5]=====
S5E01 The Cutie Map, part 1
S5E02 The Cutie Map, part 2
S5E03 Castle Sweet Castle
S5E04 Bloom and Gloom
S5E05 Tanks for the Memories
S5E06 Appleloosa's Most Wanted
S5E07 Make New Friends but Keep Discord
S5E08 The Lost Treasure of Griffonstone
S5E09 Slice of Life
S5E10 Princess Spike
S5E11 Party Pooped
S5E12 Amending Fences
S5E13 Do Princesses Dream of Magic Sheep
S5E14 Canterlot Boutique
S5E15 Rarity Investigates
S5E16 Made in Manehattan
S5E17 Brotherhooves Social
S5E18 Crusaders of the Lost Mark
S5E19 The One Where Pinkie Pie Knows
S5E20 Hearthbreakers
S5E21 Scare Master
S5E22 What About Discord
S5E23 The Hooffields and McColts
S5E24 The Mane Attraction
S5E25 The Cutie Re-Mark, part 1
S5E26 The Cutie Re-Mark, part 2

#=====[SEASON 6]=====
S6E01 The Crystalling, part 1
S6E02 The Crystalling, part 2
S6E03 The Gift of the Maud Pie
S6E04 On Your Marks
S6E05 Gauntlet of Fire
S6E06 No Second Prances
S6E07 Newbie Dash
S6E08 A Hearth's Warming Tail
S6E09 The Saddle Row Review
S6E10 Applejack Off
S6E11 Flutter Brutter
S6E12 Spice Up Your Life
S6E13 Stranger Than Fan Fiction
S6E14 The Cart Before the Ponies
S6E15 28 Pranks Later
S6E16 The Times They Are A Changeling
S6E17 Dungeons & Discords
S6E18 Buckball Season
S6E19 The Fault in Our Cutie Marks
S6E20 Viva Las Pegasus
S6E21 Every Little Thing She Does
S6E22 P.P.O.V. (Pony Point of View)
S6E23 Where the Apple Lies
S6E24 Top Bolt
S6E25 To Where and Back Again, part 1
S6E26 To Where and Back Again, part 2

#=====[SEASON 7]=====
S7E01 Celestial Advice
S7E02 All Bottled Up
S7E03 A Flurry of Emotions
S7E04 Rock Solid Friendship
S7E05 Fluttershy Leans In
S7E06 Forever Filly
S7E07 Parental Glideance
S7E08 Hard to Say Anything
S7E09 Honest Apple
S7E10 A Royal Problem
S7E11 Not Asking for Trouble
S7E12 Discordant Harmony
S7E13 The Perfect Pear
S7E14 Fame and Misfortune
S7E15 Triple Threat
S7E16 Campfire Tales
S7E17 To Change a Changeling
S7E18 Daring Done
S7E19 It Isn't the Mane Thing About You
S7E20 A Health of Information
S7E21 Marks and Recreation
S7E22 Once Upon a Zeppelin
S7E23 Secrets and Pies
S7E24 Uncommon Bond
S7E25 Shadow Play, part 1
S7E26 Shadow Play, part 2

#=====[SEASON 8]=====
S8E01 School Daze, part 1
S8E02 School Daze, part 2
S8E03 The Maud Couple
S8E04 Fake It 'Til You Make It
S8E05 Grannies Gone Wild
S8E06 Surf and-or Turf
S8E07 Horse Play
S8E08 The Parent Map
S8E09 Non-Compete Clause
S8E10 The Break Up Break Down
S8E11 Molt Down
S8E12 Marks for Effort
S8E13 The Mean 6
S8E14 A Matter of Principals
S8E15 The Hearth's Warming Club
S8E16 Friendship University
S8E17 The End in Friend
S8E18 Yakity-Sax
S8E19 Road to Friendship
S8E20 The Washouts
S8E21 A Rockhoof and a Hard Place
S8E22 What Lies Beneath
S8E23 Sounds of Silence
S8E24 Father Knows Beast
S8E25 School Raze, part 1
S8E26 School Raze, part 2

#=====[SEASON 9]=====
S9E01 The Beginning of the End, part 1
S9E02 The Beginning of the End, part 2
S9E03 Uprooted
S9E04 Sparkle's Seven
S9E05 The Point of No Return
S9E06 Common Ground
S9E07 She's All Yak
S9E08 Frenemies
S9E09 Sweet and Smoky
S9E10 Going to Seed
S9E11 Student Counsel
S9E12 The Last Crusade
S9E13 Between Dark and Dawn
S9E14 The Last Laugh
S9E15 2, 4, 6, Greaaat
S9E16 A Trivial Pursuit
S9E17 The Summer Sun Setback
S9E18 She Talks to Angel
S9E19 Dragon Dropped
S9E20 A Horse Shoe-In
S9E21 Daring Doubt
S9E22 Growing Up is Hard to Do
S9E23 The Big Mac Question
S9E24 The Ending of the End, part 1
S9E25 The Ending of the End, part 2
S9E26 The Last Problem
