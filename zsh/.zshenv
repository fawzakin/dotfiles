# Setting up basic PATH
typeset -U PATH path
export PATH="$HOME/.local/share/cargo/bin:$HOME/.local/share/flutter/bin:$HOME/.local/share/grafana/bin:$HOME/.local/share/android-sdk/Sdk/platform-tools:/usr/lib/jvm/default/bin:$HOME/.local/bin:$HOME/.local/bin/convert:$PATH"
export ANDROID_HOME="$HOME/.local/share/android-sdk"
export ANDROID_SDK_ROOT="$HOME/.local/share/android-sdk"
export DWMSTATUSBAR_SHOW_MUSIC=1

# Default Programs
export FM="thunar"
export COLORTERM="truecolor"
export EDITOR="nvim"
export READER="zathura"
export VISUAL="nvim"
export BROWSER="firefox"
export VIDEO="mpv"
export OPENER="xdg-open"
export PAGER="less"
export COMP="picom"
export PREFTOP="btop"

# Default Programs only when running under certain windowing system
if [ "$XDG_SESSION_TYPE" = "x11" ]; then
  export WM="dwm"
  export TERMINAL="st"
  export IMAGE="nsxiv-rifle"
  export LOCKER="slock"
else
  export WM="Hyprland"
  export TERMINAL="foot"
  export IMAGE="swayimg"
  export LOCKER="hyprlock"
fi

# XDG paths
export XDG_DATA_HOME=${XDG_DATA_HOME:="$HOME/.local/share"}
export XDG_CACHE_HOME=${XDG_CACHE_HOME:="$HOME/.cache"}
export XDG_CONFIG_HOME=${XDG_CONFIG_HOME:="$HOME/.config"}
#export XDG_CURRENT_DESKTOP=GNOME
#export XDG_CURRENT_DESKTOP=Hyprland

# Your Themes and scaling
#export QT_STYLE_OVERRIDE=kvantum
export QT_QPA_PLATFORMTHEME=qt5ct
export GTK_THEME=Dracula
export XCURSOR_THEME=Dracula-cursors
export XCURSOR_SIZE=24
export QT_AUTO_SCREEN_SCALE_FACTOR=0
export QT_SCALE_FACTOR=1
export QT_SCREEN_SCALE_FACTORS="1;1;1"
export GDK_SCALE=1
export GDK_DPI_SCALE=1

# Set countries for reflector
export COUNTRIES="Indonesia,Singapore,Australia,Thailand,Vietnam,Taiwan,India,Japan"

# Set the starship config location to ~/.config/zsh
export STARSHIP_CONFIG="$HOME/.config/zsh/starship.toml"

# pfetch setting
export PF_INFO="ascii title os host kernel wm shell pkgs memory palette"

# swww settings
export SWWW_TRANSITION='center'
export SWWW_TRANSITION_FPS=60
export SWWW_TRANSITION_DURATION=2

# Export wallpaper 
export WALLPAPER="$HOME/Pictures/Wallpaper/apply/main.png"
export WALLPAPER_EDP_DAY="$HOME/Pictures/Wallpaper/apply/main.png"
export WALLPAPER_EDP_NIGHT="$HOME/Pictures/Wallpaper/apply/main.png"
export WALLPAPER_HDMI_DAY="$HOME/Pictures/Wallpaper/apply/main.png"
export WALLPAPER_HDMI_NIGHT="$HOME/Pictures/Wallpaper/apply/main.png"
export LOCKPAPER="$HOME/Pictures/Wallpaper/apply/lock.png"

# History Files 
export LESSHISTFILE=-
export HISTCONTROL=ignoredups
export BOOKMARKS="$HOME/.config/bookmarks"

# Some Variables that seems "Important"
export GTK_IM_MODULE='fcitx'
export QT_IM_MODULE='fcitx'
export SDL_IM_MODULE='fcitx'
export XMODIFIERS='@im=fcitx'

# Fixing Paths
export DIALOGRC="$XDG_CONFIG_HOME"/.dialogrc
export APPIMAGE_DIR="$HOME/Programs"
export XINITRC="$HOME/.xinitrc"
export XSERVERRC="$XDG_CONFIG_HOME"/x11/xserverrc
export XRESOURCE="$XDG_CONFIG_HOME"/x11/xresources
export NPM_CONFIG_USERCONFIG=$XDG_CONFIG_HOME/npm/npmrc
export PASSWORD_STORE_DIR="$HOME/.local/pass"
export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc
export GOPATH="$XDG_DATA_HOME"/go
export ZDOTDIR="$XDG_CONFIG_HOME"/zsh
export HISTFILE="$XDG_CACHE_HOME"/zsh_history
export CARGO_HOME="$XDG_DATA_HOME"/.local/share/cargo
export RIPGREP_CONFIG_PATH="$XDG_CONFIG_HOME/ripgrep/ripgreprc"
export AWT_TOOLKIT=MToolkit
export JAR=/path/to/eclipse.jdt.ls/org.eclipse.jdt.ls.product/target/repository/plugins/org.eclipse.equinox.launcher_1.6.0.v20200915-1508.jar
export GRADLE_HOME="$XDG_CACHE_HOME"/gradle
export JAVA_HOME=$(readlink -f /usr/bin/java | sed "s:/bin/java::")
export JDTLS_CONFIG=$HOME/.local/share/nvim/lspinstall/java/config_linux
export CHROME_EXECUTABLE=chromium
export RUSTUP_HOME=$HOME/.local/share/.rustup

# Fix for OnlyOffice and Java programs. Comment if they cause trouble
export QT_QPA_PLATFORM=xcb
export _JAVA_AWT_WM_NONREPARENTING=1
export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME"/java

# Something to do with terminal blinking idk.
export LESS_TERMCAP_mb=$(tput bold; tput setaf 2)            # green - Start blinking
export LESS_TERMCAP_md=$(tput bold; tput setaf 2)            # green - Start bold
export LESS_TERMCAP_so=$(tput bold; tput setaf 3)            # yellow - Start stand out
export LESS_TERMCAP_us=$(tput smul; tput bold; tput setaf 1) # red - Start underline
export LESS_TERMCAP_se=$(tput rmso; tput sgr0)               # End standout
export LESS_TERMCAP_ue=$(tput sgr0)                          # End Underline
export LESS_TERMCAP_me=$(tput sgr0)                          # End bold, blinking, standout, underline

# lf icons
export LF_ICONS="\
di=:\
fi=:\
ln=:\
or=:\
*.vimrc=:\
*.viminfo=:\
*.gitignore=:\
*.c=:\
*.cc=:\
*.clj=:\
*.coffee=:\
*.cpp=:\
*.css=:\
*.d=:\
*.dart=:\
*.erl=:\
*.exs=:\
*.fs=:\
*.go=:\
*.h=:\
*.hh=:\
*.hpp=:\
*.hs=:\
*.html=:\
*.java=:\
*.jl=:\
*.js=:\
*.json=:\
*.lua=:\
*.md=:\
*.php=:\
*.pl=:\
*.pro=:\
*.py=:\
*.rb=:\
*.rs=:\
*.scala=:\
*.ts=:\
*.vim=:\
*.cmd=:\
*.ps1=:\
*.sh=:\
*.bash=:\
*.zsh=:\
*.fish=:\
*.tar=:\
*.tgz=:\
*.arc=:\
*.arj=:\
*.taz=:\
*.lha=:\
*.lz4=:\
*.lzh=:\
*.lzma=:\
*.tlz=:\
*.txz=:\
*.tzo=:\
*.t7z=:\
*.zip=:\
*.z=:\
*.dz=:\
*.gz=:\
*.lrz=:\
*.lz=:\
*.lzo=:\
*.xz=:\
*.zst=:\
*.tzst=:\
*.bz2=:\
*.bz=:\
*.tbz=:\
*.tbz2=:\
*.tz=:\
*.deb=:\
*.rpm=:\
*.jar=:\
*.war=:\
*.ear=:\
*.sar=:\
*.rar=:\
*.alz=:\
*.ace=:\
*.zoo=:\
*.cpio=:\
*.7z=:\
*.rz=:\
*.cab=:\
*.wim=:\
*.swm=:\
*.dwm=:\
*.esd=:\
*.jpg=:\
*.jpeg=:\
*.mjpg=:\
*.mjpeg=:\
*.gif=:\
*.bmp=:\
*.pbm=:\
*.pgm=:\
*.ppm=:\
*.tga=:\
*.xbm=:\
*.xpm=:\
*.tif=:\
*.tiff=:\
*.png=:\
*.svg=:\
*.svgz=:\
*.mng=:\
*.pcx=:\
*.mov=:\
*.mpg=:\
*.mpeg=:\
*.m2v=:\
*.mkv=:\
*.webm=:\
*.ogm=:\
*.mp4=:\
*.m4v=:\
*.mp4v=:\
*.vob=:\
*.qt=:\
*.nuv=:\
*.wmv=:\
*.asf=:\
*.rm=:\
*.rmvb=:\
*.flc=:\
*.avi=:\
*.fli=:\
*.flv=:\
*.gl=:\
*.dl=:\
*.xcf=:\
*.xwd=:\
*.yuv=:\
*.cgm=:\
*.emf=:\
*.ogv=:\
*.ogx=:\
*.aac=:\
*.au=:\
*.flac=:\
*.m4a=:\
*.mid=:\
*.midi=:\
*.mka=:\
*.mp3=:\
*.mpc=:\
*.ogg=:\
*.ra=:\
*.wav=:\
*.oga=:\
*.opus=:\
*.spx=:\
*.xspf=:\
*.pdf=:\
*.nix=:\
"
