# My config of zsh.
# Some of this lines are ripped off from Luke Smith's and Brodie Robertson's zsh config.

# Enable colors
autoload -U colors && colors	

# History variables
[ ! -f "${XDG_CACHE_HOME}/zsh_history" ] && mkdir -p ${XDG_CACHE_HOME} && touch ${XDG_CACHE_HOME}/zsh_history
HISTFILE=${XDG_CACHE_HOME}/zsh_history
HISTSIZE=10000
SAVEHIST=10000

# Auto completion
autoload -U compinit && compinit -u
zstyle ':completion:*' menu select

# Auto complete with case insenstivity. Include hidden files
zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'
zmodload zsh/complist
compinit
_comp_options+=(globdots)

# Vim mode
bindkey -v
export KEYTIMEOUT=1

# Enable searching through history
bindkey '^R' history-incremental-pattern-search-backward

# Use vim keys in tab complete menu:
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history

# And arrow keys as well:
bindkey -M menuselect 'left' vi-backward-char
bindkey -M menuselect 'down' vi-down-line-or-history
bindkey -M menuselect 'up' vi-up-line-or-history
bindkey -M menuselect 'right' vi-forward-char

# Fix bug when switching mode
bindkey -v '^?' backward-delete-char

# Edit line in vim buffer ctrl-e
autoload edit-command-line; zle -N edit-command-line; bindkey '^e' edit-command-line

# Enter vim buffer from normal mode
autoload -U edit-command-line && zle -N edit-command-line && bindkey -M vicmd "^e" edit-command-line

# Change cursor shape for different vi modes.
function zle-keymap-select () {
    case $KEYMAP in
        vicmd) echo -ne '\e[1 q';;      # block
        viins|main) echo -ne '\e[5 q';; # beam
    esac
}

zle -N zle-keymap-select
zle-line-init() {
    zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
    echo -ne "\e[5 q"
}

zle -N zle-line-init
echo -ne '\e[5 q'                # Use beam shape cursor on startup.
preexec() { echo -ne '\e[5 q' ;} # Use beam shape cursor for each new prompt.

# Keybinding for programs
bindkey -s "^k" "clear\n"

# Open lf using ctrl+p
lfcd() {
    tmp="$(mktemp)"
    lf -last-dir-path="$tmp" "$@"
    echo $tmp
    if [ -f "$tmp" ]; then
        dir="$(/bin/cat "$tmp")"
        rm -f "$tmp" > /dev/null
        [ -d "$dir" ] && [ "$dir" != "$(pwd)" ] && cd "$dir"
    fi
}
bindkey -s '^p' 'lfcd\n'

# Other
unsetopt beep		# Disable PC speaker sound
setopt autocd		# Automatically cd into typed directory.
stty stop undef		# Disable ctrl-s to freeze terminal.
setopt interactive_comments

# Load aliases. Edit aliases by running "zalias"
[ -f "${XDG_CONFIG_HOME:-$HOME/.config}/zsh/.zalias" ] && source "${XDG_CONFIG_HOME:-$HOME/.config}/zsh/.zalias"

# Fix Highlight color in tmux.
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=8"

# Load usefull zsh plugins
zshplug="$HOME/.local/share/zsh-plugins"
[ -f ${zshplug}/fast/fast-syntax-highlighting.plugin.zsh ] && source ${zshplug}/fast/fast-syntax-highlighting.plugin.zsh 2>/dev/null
[ -f ${zshplug}/auto/zsh-autosuggestions.zsh ] && source ${zshplug}/auto/zsh-autosuggestions.zsh 2>/dev/null

# Command not found
[ -f /usr/share/doc/pkgfile/command-not-found.zsh ] && source /usr/share/doc/pkgfile/command-not-found.zsh 2>/dev/null

# Other command line utilities
# Starship config: ~/.config/zsh/starship.toml
eval "$(starship init zsh)"        || echo "Starship fails to load/is not installed"
eval "$(atuin init zsh)"           || echo "Atuin fails to load/is not installed"
eval "$(zoxide init --cmd cd zsh)" || echo "Zoxide fails to load/is not installed"
