#!/bin/zsh
[ -x "$(command -v systemctl)" ] && systemctl --user import-environment PATH
if [ "$(tty)" = "/dev/tty1" ]; then
  Hyprland &
  wait $! > /dev/null && exit
fi
