-- Basic Options
O.wrap = false
O.mouse = 'a'
O.title = true
O.ruler = true
O.hidden = true
O.number = true
O.confirm = true
O.undofile = true
O.wildmenu = true
O.showmode = false
O.incsearch = true
O.smartcase = true
O.ignorecase = true
O.cursorline = true
O.startofline = false
O.termguicolors = true
O.relativenumber = true

-- Buffer Options
Opt.cmdheight = 1
Opt.scrolloff = 4
Opt.laststatus = 4
Opt.showtabline = 2

-- Indenting
Opt.expandtab = true
Opt.smartindent = true
Opt.breakindent = true
Opt.shiftwidth = 2
Opt.tabstop = 4
Opt.softtabstop = 4

-- Splitting
O.splitright = true
O.splitbelow = true

-- Timeout time
O.timeout = true
O.updatetime = 200
O.timeoutlen = 200

-- Other
O.swapfile = true
O.clipboard = 'unnamedplus'
O.signcolumn = 'yes'
O.completeopt = 'menuone,noselect'
Opt.backspace = { 'indent', 'eol', 'start' }
Opt.fillchars = { eob = " " }
Opt.whichwrap:append "<>[]hl"

-- Spell Checking (Toggle with SPC l)
O.spelllang = 'en_us'
O.spell = false
