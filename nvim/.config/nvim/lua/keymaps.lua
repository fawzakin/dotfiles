G.mapleader = ' '
G.maplocalleader = ' '

-- Resize Window
Map('n', '<M-C-j>', true, 'resize -2', "Decrease Horizontal buffer size")
Map('n', '<M-C-k>', true, 'resize +2', "Increase Horizontal buffer size")
Map('n', '<M-C-h>', true, 'vertical resize -1', "Decrease vertical buffer size")
Map('n', '<M-C-l>', true, 'vertical resize +1', "Increase vertical buffer size")

-- Switch split buffer
Map('n', '<C-j>', false, '<C-w>j', "Go to below buffer")
Map('n', '<C-k>', false, '<C-w>k', "Go to above buffer")
Map('n', '<C-h>', false, '<C-w>h', "Go to left buffer")
Map('n', '<C-l>', false, '<C-w>l', "Go to right buffer")

-- Moving Tab
Map('n', '<S-TAB>', true, 'tabn', "Switch to next tab")
Map('n', '_', true, 'tabp', "Switch to previous tab")
Map('n', '+', true, 'tabn', "Switch to next tab")
Map('n', '<M-->', true, '-tabmove', "Move to previous tab")
Map('n', '<M-=>', true, '+tabmove', "Move to next tab")

-- Easy CAPS
Map('n', '<leader>c', false, 'viwg~i<Esc>', "Switch capitilization")

-- Add Indentation
Map('v', '>', false, '>gv')
Map('v', '<', false, '<gv')

-- Replace line after/before the selected line
Map('n', '<Leader>o', false, 'j<Esc>^Da', "Replace next line")
Map('n', '<Leader>i', false, 'k<Esc>^Da', "Replace previous line")

-- Clear Highlighting
Map('n', '<Leader>p', true, 'nohl', "Clear Highlighting")

-- Escape from Terminal
Map('t', '<C-n>', false, '<C-\\><C-n>')

-- Config Stuff
local conf, _ = Get_config_path()
Map('n', '<leader>r', true, 'source ' .. conf, "Reload Config")
Map('n', '<leader>ec', true, 'e ' .. conf, "Edit Config")

-- Toggles
vim.keymap.set('n', '<M-w>', function() Auto_wrap_toggle() end, { desc = "Toggle text wrap" })
vim.keymap.set('n', '<leader>cc', function() Spell_check_toggle() end, { desc = "Toggle spell [c]heck" })

-- Abbreviate 'W' and 'Q' to 'w' and 'q' because I keep capitalizing those by mistake. Idk how I can convert this to Lua.
vim.cmd([[
  fun! SetupCommandAlias(from, to)
    exec 'cnoreabbrev <expr> '.a:from
      \ .' ((getcmdtype() is# ":" && getcmdline() is# "'.a:from.'")'
      \ .'? ("'.a:to.'") : ("'.a:from.'"))'
  endfun
  call SetupCommandAlias("W","w")
  call SetupCommandAlias("Wq","wq")
  call SetupCommandAlias("Q","q")
]])

-- Plugins with keybinding:
Map('n', '<C-n>', true, 'NvimTreeFindFileToggle', "Open file explorer sidebar")
Map('n', '<leader>u', true, 'UndotreeToggle', "Open Undo Tree")
Map('n', '<leader>dd', true, 'Alpha', "Open Dashboard")
Map('t', '<C-x>', false, vim.api.nvim_replace_termcodes("<C-\\><C-N>", true, true, true), "Escape Terminal Mode")

-- Plugins with complicated keybinding setup:
-- ~/.config/nvim/lua/config/lsp.lua
-- ~/.config/nvim/lua/config/misc.lua
-- ~/.config/nvim/lua/config/melescope.lua
-- ~/.config/nvim/lua/config/tree.lua
-- ~/.config/nvim/lua/config/treesitter.lua
