-- Define color for every file to use
-- Custom Onedark theme
require('onedark').setup {
    style = 'darker',
    colors = { -- Override default colors
      bg0 = Bgcol0,
      bg1 = Bgcol1,
    },
}

-- Change your color scheme here:
vim.cmd.colorscheme 'onedark'

-- Indent line config
local hl0 = string.format("highlight LightIndent guifg=%s gui=nocombine", Incol)
vim.cmd(hl0)

require("ibl").setup {
    indent = { char = "┊" },
    whitespace = { remove_blankline_trail = false },
    scope = {
        highlight = "LightIndent",
        show_start = false,
        show_end = false,
    },
}

G.indent_blankline_filetype_exclude = {'dashboard'}
