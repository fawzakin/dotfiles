require("config.theme")     -- Always put the theme first
require("config.alpha")
require("config.line")
require("config.lsp")
require("config.misc")
require("config.telescope")
require("config.tree")
require("config.treesitter")
require("config.tex")
