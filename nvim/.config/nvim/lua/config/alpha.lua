-- Credit to AdamWhittingham for giving an example on how to configure this thing
-- https://github.com/goolord/alpha-nvim/discussions/16#discussioncomment-1927405

local alpha = require("alpha")
local plug = require('lazy').stats().loaded + 1

local emblem = {
  '    ▄▄               ▄██▄               ▄▄    ',
  '   ▄█▀        ██▄  ▄▄█▒▒█▄▄  ▄██        ▀█▄   ',
  '  ▄█▀          ▀███▒▒▒▒▒▒▒▒███▀          ▀█▄  ',
  ' ▄█▒            ▄█▒⣤⣿⣤   ⣿⣿▒█▄            ▒█▄ ',
  '▄█▒▒         ▄██▒▒▒⣿⣿⣿⣿⣤ ⣿⣿▒▒▒██▄         ▒▒█▄',
  '▀█▒▒         ▀██▒▒▒⣿⣿ ⠛⣿⣿⣿⣿▒▒▒██▀         ▒▒█▀',
  ' ▀█▒            ▀█▒⣿⣿   ⠛⣿⠛▒█▀            ▒█▀ ',
  '  ▀█▄          ▄███▒▒▒▒▒▒▒▒███▄          ▄█▀  ',
  '   ▀█▄        ██▀  ▀▀█▒▒█▀▀  ▀██        ▄█▀   ',
  '    ▀▀               ▀██▀               ▀▀    ',
  '',
}

local splash = {
  'Welcome to Neovim, the best text editor',
  'Vim: Scaring Windows users since 1991',
  'Neovim 2: The Rise of the Codes',
  'Neovim 2: Electric Boogaloo',
  'Neovim: Gaze deeply into unknown regions using the power of the moon',
  'Neovim: It\'s a pleasure to meet you. Oh wait. No, it\'s not.',
  'Neovim: Loaded ' .. plug .. ' plugins (important Info)',
  'Neovim: The sequel of Vim, which is the sequel to vi',
  'Neovim: My thoughts will follow you into your buffer',
  'Neovim: Alpha male doesn\'t exist, alpha-nvim does',
  'Neovim: What does it matter how my config breaks?',
  'Neovim: Slow dancing in the One Dark colorscheme',
  'Neovim: Good to see you, you are my rolling star',
  'Neovim: It\'s what you accomplish with your life',
  'Neovim: Good editors copy, great editors steal',
  'Neovim: I don\'t watch anime (shocking I know)',
  'Neovim: The free and open source text editor',
  'Neovim: Don\'t forget to update your system',
  'Neovim: Don\'t forget to charge your laptop',
  'Neovim: It is now safe to quit your editor',
  'Neovim: Stupid bug, you made me look bad',
  'Neovim: Because Emacs is too complicated',
  'Neovim: And Stanley pushed the e button',
  'Neovim: I can\'t believe it\'s not nano!',
  'Neovim: Neovim: Neovim: Neovim: Neovim:',
  'Neovim: Typing at the speed of sound',
  'Neovim: Somebody toucha my config',
  'Neovim: May the forks be with you',
  'Neovim: Made in C (and with love)',
  'Neovim: The industrial revolution',
  'Neovim: Oh my god, they killed vi',
  'Neovim: A Compiling argument...',
  'Neovim: Beep Beep I\'m a sheep',
  'Neovim: My pronouns are vi/vim',
  'Neovim: Blue Eyes Green Dragon',
  'Neovim: And I did it my way!',
  'Neovim: Follow the line, CJ!',
  'Neovim: Works great on Unix',
  'Neovim: Long live the Atom',
  'Neovim: Better than VSCode',
  'Neovim: I wanna be the GUI',
  'Neovim: Fly me to the moon',
  'Neovim: Pose for the fans!',
  'Neovim: It\'s in the name',
  'Neovim: Also try Kakoune',
  'Neovim: Also try Lapce',
  'Neovim: Also try Helix',
  'Neovim: Attack on Lua',
  'Neovim: Packer Man',
  'Neobim: A typo',
  'Configuring with Vimscript almost makes you wish for a nuclear winter',
  'I could tell you how to exit Vim, but then I\'d have to kill you',
  'The current time is ' .. os.date("%H:%M") .. ' (you\'re welcome)',
  '[[ Insert inspiring quote from well-known person here ]]',
  'Congratulations! Your Vim evolved into Neovim!',
  'local function free_heart() return \' \' end',
  'You do not recognize the bodies in the /var/water',
  'Type \'ZZ\' to quit Neovim. Thank me later!',
  'You will configure Neovim once more',
  'Fun Fact: \'Vim\' is an actual noun',
  'You like using Neovim, don\'t you?',
  'What is this editor called again?',
  'Goodbye Notepad++. Hello Neovim!',
  'Goolord, what\'s happening here?',
  'The Neovim Parable: Ultra Deluxe',
  'Vimon, Digivolve to Neovimon!',
  ' Free heart for everyone ',
  'Neovim Genesis Evangelion',
  'Ecce Vim Quod Est Novus',
  'SUNNY... I love you...',
  'ネオビムナンバーワン',
  '=== N E O V I M ===',
  'The Cult of Neovim',
  'Dr. Neovim Cortext',
  'It\'s Neovim Time!',
  'Gone Vimsexual',
  ' <- Cool Logo',
  '󰎖     󱘊 ',
  '󰄛 Meowvim 󰄛',
  'Neovim UwU',
}

local emblem_text = {
  type = "text",
  val = emblem,
  opts = {
      position = "center",
      hl = "Type",
  },
}

local splash_text = {
  type = "text",
  val = splash[math.random(#splash)],
  -- val = splash[32], -- For testing purpose
  opts = {
      position = "center",
      hl = "Constant",
  },
}

local header = {
  type = "group",
  val = { emblem_text, splash_text },
  opts = {
    position = "center",
  }
}

local _, os_name = Get_config_path()
local v = vim.version()
local v_nvim = "v" .. v.major .. "." .. v.minor .. "." .. v.patch

local footer = {
  type = "text",
  val = " " .. os.date("%A, %B %d %Y") .. " |  " .. plug .. " plugins | " .. os_name ..  " | " .. v_nvim,
  opts = {
    position = "center",
    hl = "Special",
  }
}

local theme = require("alpha.themes.theta")
local config = theme.config
local dashboard = require("alpha.themes.dashboard")
local buttons = {
    type = "group",
    val = {
        { type = "text", val = "Quick links", opts = { hl = "SpecialComment", position = "center" } },
        dashboard.button("e", "  New file", "<cmd>ene<CR>"),
        dashboard.button("u", "  Update plugins", "<cmd>Lazy sync<CR>"),
        dashboard.button("SPC ec", "  Edit Config"),
        dashboard.button("SPC sf", "  Search Files"),
        dashboard.button("SPC sg", "  Search Grep"),
    },
    position = "center",
}

config.layout[1] = { type = "padding", val = 1 }
config.layout[2] = header
config.layout[3] = { type = "padding", val = 1 }
config.layout[5] = { type = "padding", val = 1 }
config.layout[6] = buttons
config.layout[7] = { type = "padding", val = 1 }
config.layout[8] = footer
alpha.setup(config)
