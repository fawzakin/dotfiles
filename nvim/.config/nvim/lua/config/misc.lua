-- The misc config is for plugins with very little configuration to add

-- gitsigns
require('gitsigns').setup({
  signs = {
    add = { text = '+' },
    change = { text = '~' },
    delete = { text = '_' },
    topdelete = { text = '‾' },
    changedelete = { text = '~' },
  },
  on_attach = function(bufnr)
    vim.keymap.set('n', '[c', require('gitsigns').prev_hunk, { buffer = bufnr, desc = 'Go to Previous Hunk' })
    vim.keymap.set('n', ']c', require('gitsigns').next_hunk, { buffer = bufnr, desc = 'Go to Next Hunk' })
    vim.keymap.set('n', '<leader>ph', require('gitsigns').preview_hunk, { buffer = bufnr, desc = '[P]review [H]unk' })
  end,
})

-- Peek (Preview Markdown)
require('peek').setup({
  auto_load = false,
  close_on_bdelete = true,
  syntax = true,
  theme = 'dark',
  update_on_change = true,
  app = 'vimb',
  filetype = { 'markdown' },
  throttle_at = 200000,
  throttle_time = 'auto',
})

vim.api.nvim_create_user_command('MarkdownPreview', function()
  local peek = require('peek')
  if peek.is_open() then
    peek.close()
    return
  end
  peek.open()
end, {})

Map('n', '<leader>m', true, 'MarkdownPreview', "Preview Markdown in browser")

-- nvterm
require("nvterm").setup({
  terminals = {
    shell = vim.o.shell,
    list = {},
    type_opts = {
      float = {
        relative = 'editor',
        row = 0.3,
        col = 0.25,
        width = 0.5,
        height = 0.4,
        border = "single",
      },
      horizontal = { location = "rightbelow", split_ratio = .3, },
      vertical = { location = "rightbelow", split_ratio = .5 },
    }
  },
  behavior = {
    autoclose_on_quit = {
      enabled = false,
      confirm = true,
    },
    close_on_exit = true,
    auto_insert = true,
  },
})

local term = require("nvterm.terminal")
vim.keymap.set('n', '<C-t>', function() term.toggle "horizontal" end, { desc = "Toggle Terminal Window" })
