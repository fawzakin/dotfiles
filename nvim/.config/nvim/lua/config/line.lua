-- lualine config
require('lualine').setup {
  options = {
    icons_enabled = true,
    theme = GlobalTheme,
    component_separators = { left = '|', right = '|'},
    section_separators = { left = '', right = ''},
    globalstatus = true,
    refresh = { statusline = 1000 }
  },
  sections = {
    lualine_a = { { 'mode', separator = { left = '', right = '' }, right_padding = 2 } },
    lualine_b = { 'filename', 'branch' },
    lualine_c = { 'diff' },
    lualine_x = { 'diagnostics' },
    lualine_y = { 'filetype', 'progress' },
    lualine_z = { { 'location', separator = { left = '', right = '' }, left_padding = 2, right_padding = 2 } },
  },
  extensions = { 'nvim-tree' }
}

-- Tabby Config
-- Credit: 007Psycho007 [https://github.com/nanozuki/tabby.nvim/discussions/106]
local function title_rename(name)
    if string.find(name,"NvimTree") then
      return "Files"
    elseif string.find(name, "No Name") then
      return "New"
    end
    return name
end

local function tab_name(tab)
  local clean = string.gsub(tab,"%[..%]","")
  return title_rename(clean)
end

local function tab_modified(tab)
  local wins = require("tabby.module.api").get_tab_wins(tab)
  for _, x in pairs(wins) do
    if vim.bo[vim.api.nvim_win_get_buf(x)].modified then
      return ""
    end
  end
  return ""
end

local function number_delim(num, delim)
  return tostring(num) .. delim
end

local function name_truncate(name)
  if #name > 5 then
    return string.sub(name, 1, 5) .. ""
  end
  return name
end

local function lsp_diag(buf)
    local diagnostics = vim.diagnostic.get(buf)
    local count = {0, 0, 0, 0}
    for _, diagnostic in ipairs(diagnostics) do
        count[diagnostic.severity] = count[diagnostic.severity] + 1
    end
    if count[1] > 0 then
        return vim.bo[buf].modified and "" or ""
    elseif count[2] > 0 then
        return vim.bo[buf].modified and "" or ""
    end
    return vim.bo[buf].modified and "" or ""
end

local theme = {
  fill = { bg=Bgbar },
  current = { bg=Bgcol0 },
  inactive = { bg=Bgbar },
  slwin = { fg=Sltxt, bg=Slbar },
  unwin = { fg=Untxt, bg=Unbar },
}

require('tabby.tabline').set(function(line)
  return {
    line.sep('', theme.fill, theme.current),
    line.tabs().foreach(function(tab)
      local hl = tab.is_current() and theme.current or theme.inactive
      return {
        line.sep('', hl, theme.fill),
        number_delim(tab.number(), ":"),
        name_truncate(tab_name(tab.name())),
        tab_modified(tab.id),
        line.sep('', hl, theme.fill),
        hl = hl,
        margin = ' ',
      }
    end),
    line.spacer(),
    line.wins_in_tab(line.api.get_current_tab()).foreach(function(win)
      local hl = win.is_current() and theme.slwin or theme.unwin
      return {
        line.sep('', theme.fill, hl),
        win.file_icon(),
        title_rename(win.buf_name()),
        lsp_diag(win.buf().id),
        line.sep('', hl, theme.fill),
        hl = hl,
        margin = ' ',
      }
    end),
    line.sep('', theme.fill, theme.current),
    hl = theme.fill,
  }
end)
