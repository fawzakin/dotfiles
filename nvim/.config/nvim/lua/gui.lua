-- Neovide config
if G.neovide then
  O.guifont = "JetBrainsMono NF Medium:h15"
  G.neovide_fullscreen = false
  G.neovide_remember_window_size = false
  G.neovide_cursor_animation_length = 0.05
  G.neovide_cursor_vfx_mode = "ripple"
end
