-- This is a helper script to help other settings
-- It doesn't configure Neovim on its own

-- Simplify basic options
G = vim.g
O = vim.o
Opt = vim.opt

-- Simplify mapping function
function Map(mod, key, is_vim_cmd, fun, des)
  local opts = {
    desc = des,
    noremap = true,
    silent = true
  }
  if is_vim_cmd then
    vim.keymap.set(mod, key, function() vim.cmd(fun) end, opts)
  else
    vim.keymap.set(mod, key, fun, opts)
  end
end

-- Easily get config path and OS name for keybinding and dashboard
function Get_config_path()
  local os_name = package.config:sub(1,1)
  local conf_path = '~/.config/nvim/init.lua'

  if os_name == '/' then
    os_name = ' Unix'
  else
    conf_path = '~\\AppData\\Local\\nvim\\init.lua'
    os_name = ' Windows'
  end

  return conf_path, os_name
end

-- Toggleable autowrap function
function Auto_wrap_toggle()
  local keybind = { 'j', 'k', '$', '0' }
  if O.wrap then -- If wrap, turn it off
    print("Text wrap disabled")
    O.wrap = false
    for _, kb in ipairs(keybind) do
      vim.keymap.del('n', kb)
    end
  else
    print("Text wrap enabled")
    O.wrap = true
    for _, kb in ipairs(keybind) do
      vim.keymap.set('n', kb, 'g' .. kb)
    end
  end
end

-- Toggleable spell checking function
function Spell_check_toggle()
  if O.spell then -- if spell, turn it off
    print("Spell checking disabled")
    Opt.spell = false
  else
    print("Spell checking enabled")
    O.spell = true
  end
end


