local lazypath = vim.fn.stdpath 'data' .. '/lazy/lazy.nvim'
if not vim.loop.fs_stat(lazypath) then
  print([[
    Installing lazy.nvim plugin
    Please wait...
  ]])
  vim.fn.system {
    'git',
    'clone',
    '--filter=blob:none',
    'https://github.com/folke/lazy.nvim.git',
    '--branch=stable',
    lazypath,
  }
end
Opt.rtp:prepend(lazypath)

require('lazy').setup({
  -- =====[[ Dev/Git ]]=====--
  { -- LSP Configuration & Plugins
    'neovim/nvim-lspconfig',
    dependencies = {
      { 'williamboman/mason.nvim', config = true },
      { 'williamboman/mason-lspconfig.nvim' },
      { 'j-hui/fidget.nvim', opts = {} },
      { 'folke/neodev.nvim' },
    },
  },

  { -- Autocompletion
    'hrsh7th/nvim-cmp',
    dependencies = {
      'L3MON4D3/LuaSnip',
      'saadparwaiz1/cmp_luasnip',
      'hrsh7th/cmp-nvim-lsp',
      'rafamadriz/friendly-snippets',
    },
  },

  { -- Highlight, edit, and navigate code
    'nvim-treesitter/nvim-treesitter',
    dependencies = { 'nvim-treesitter/nvim-treesitter-textobjects' },
    build = ':TSUpdate',
  },

  { -- Which key are you going to use?
    "folke/which-key.nvim",
    init = function()
      O.timeout = true
      O.timeoutlen = 300
    end,
  },

  -- Git stuff
  'lewis6991/gitsigns.nvim',
  'f-person/git-blame.nvim',
  'tpope/vim-fugitive',
  'tpope/vim-rhubarb',

  -- =====[[ File Manager/Finder ]]=====--
  { -- File manager for Neovim
    'nvim-tree/nvim-tree.lua',
    dependencies = { 'nvim-tree/nvim-web-devicons' }
  },

  { -- Finder function
    'nvim-telescope/telescope.nvim',
    branch = '0.1.x',
    dependencies = { 'nvim-lua/plenary.nvim' }
  },

  { -- use native fzf program for telescope
    'nvim-telescope/telescope-fzf-native.nvim',
    build = 'make',
    cond = function()
      return vim.fn.executable 'make' == 1
    end,
  },

  { -- Cool startup page for Neovim (includes icons too)
    'goolord/alpha-nvim',
    lazy = true,
    requires = { 'kyazdani42/nvim-web-devicons' },
  },

  -- =====[[ Visual Aids ]]=====--
  -- { 'toppair/peek.nvim', build = 'deno task --quiet build:fast' },  -- Broken at the moment
  { 'saimo/peek.nvim', run = 'deno task --quiet build:fast', opts = {} },  -- Markdown preview using external browser, Require deno
  { 'lukas-reineke/indent-blankline.nvim', main = "ibl", opts = {} },             -- Indentation guide
  { 'NvChad/nvim-colorizer.lua', opts = {} },                       -- Display color code directly on neovim
  'machakann/vim-highlightedyank',  -- Highlight yanked/copied text
  'psliwka/vim-smoothie',           -- Smooth Scrolling with ctrl+{u/d (small), b/f (big)}

  -- =====[[ Functions ]]=====--
  { 'numToStr/Comment.nvim', opts = {} }, -- Turn a line into a comment. "gcc" to comment
  { 'windwp/nvim-autopairs', opts = {} }, -- Autopair Paranthesis, Bracket, etc
  'equalsraf/neovim-gui-shim',-- Fix :Gui command for neovim GUI (useful on Windows)
  'farmergreg/vim-lastplace', -- Save cursor location on quit
  'junegunn/vim-easy-align',  -- Auto align text using gaip
  'dkarter/bullets.vim',      -- Auto bullet point for Markdown
  'tpope/vim-markdown',       -- Better Markdown syntax highlight than treesitter
  'tpope/vim-surround',       -- Surround words with a character
  'tpope/vim-sleuth',         -- Detect tabstop and shiftwidth automatically
  'tpope/vim-repeat',         -- Repeat plugin commands with '.'
  'mbbill/undotree',          -- Navigate undo history easily
  'NvChad/nvterm',            -- Quickly setup Neovim's build-in terminal
  'lervag/vimtex',            -- Latex stuff

  -- { -- Multiline cursor like Sublime Text in case I "need" this later
  --  'mg979/vim-visual-multi',
  --  init = function()
  --    vim.cmd([[
  --      let g:VM_maps = {}
  --      let g:VM_maps['Find Under']         = '<C-i>'     " replace C-n
  --      let g:VM_maps['Find Subword Under'] = '<C-i>'     " replace visual C-n
  --      let g:VM_maps["Select Cursor Down"] = '<C-Down>'  " start selecting down
  --      let g:VM_maps["Select Cursor Up"]   = '<C-Up>'    " start selecting up
  --    ]])
  --  end
  -- },

  -- =====[[ Themes ]]=====--
  'navarasu/onedark.nvim',
  'nvim-lualine/lualine.nvim',
  'nanozuki/tabby.nvim',
})

require("config")

