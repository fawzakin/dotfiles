-- Inspiration Config: 
-- https://github.com/nvim-lua/kickstart.nvim/blob/master/init.lua
-- https://github.com/Aylur/dotfiles/tree/main/.config/nvim
-- Use <C-n> to navigate through settings
-- Must be in this order: helper -> color/gui -> settings -> keymaps -> plugins 

require("helper")   -- ~/.config/nvim/lua/helper.lua
require("color")    -- ~/.config/nvim/lua/color.lua
require("gui")      -- ~/.config/nvim/lua/gui.lua
require("settings") -- ~/.config/nvim/lua/settings.lua
require("keymaps")  -- ~/.config/nvim/lua/keymaps.lua
require("plugins")  -- ~/.config/nvim/lua/plugins.lua
