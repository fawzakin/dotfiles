" nvim-qt/fvim config
if exists(':GuiFont')
    " Use GuiFont! to ignore font errors
    GuiFont JetBrainsMono\ NF\ Medium:h14
endif

" Enable Ligatures
if exists(':GuiRenderLigatures')
    GuiRenderLigatures 1
endif

" Disable GUI Tabline
if exists(':GuiTabline')
    GuiTabline 0
endif

" Disable GUI Popupmenu
if exists(':GuiPopupmenu')
    GuiPopupmenu 0
endif

" fvim specific config
if exists('g:fvim_loaded')
    " Ctrl-ScrollWheel for zooming in/out
    nnoremap <silent> <C-ScrollWheelUp> :set guifont=+<CR>
    nnoremap <silent> <C-ScrollWheelDown> :set guifont=-<CR>
    nnoremap <A-CR> :FVimToggleFullScreen<CR>

    " Some fancy cursor effects
    FVimCursorSmoothMove v:true
    FVimCursorSmoothBlink v:true
endif
