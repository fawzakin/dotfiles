" Fawzan's basic Neovim config. Should be compatible with regular Vim.
" NOTE: ginit.vim
" Some settings are taken from the following sources:
" https://vim.fandom.com/wiki/Example_vimrc
" https://www.chrisatmachine.com/Neovim/01-vim-plug/
" https://www.chrisatmachine.com/Neovim/02-vim-general-settings/

" Table of content
" (1). Basic Settings
" (2). Keybinding Settings
" (3). Plugin Settings
" (4). Gui Settings

" ========================
" || (1) Basic Settings || 
" ========================
set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath = &runtimepath
set nocompatible

syntax enable#
set mouse=a
set t_Co=256
set hidden nowrap noshowmode nostartofline wildmenu showcmd hlsearch ruler title confirm 
set autoindent smartindent ignorecase smartcase number relativenumber cursorline laststatus=2
set splitright splitbelow 
set backspace=indent,eol,start
set visualbell t_vb=
set cmdheight=1 showtabline=1 
set ttimeout ttimeoutlen=200
set pastetoggle=<F11> 
set shiftwidth=4 softtabstop=4 expandtab
set encoding=utf-8 fileencoding=utf-8  
set iskeyword+=-
set clipboard+=unnamedplus
au! BufWritePost $MYVIMRC source %
if has('filetype') 
  filetype indent plugin on
endif

" =============================
" || (2) Keybinding Settings || 
" =============================

" Keybinding for plugins are in plugin settings
" map explanations:
" map: Map a key.
" noremap: Map a key non-recursively. All Mode
" nnoremap: Map a key non-recursively. Normal mode.
" inoremap: Map a key non-recursively. Insert mode.
" vnoremap: Map a key non-recursively. Visual mode.
" Change the leader key to space. Useful for custom commands.
let mapleader = " "

" Better nav for omnicomplete
inoremap <expr> <c-j> ("\<C-n>")
inoremap <expr> <c-k> ("\<C-p>")

" Better window navigation
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" Use alt + hjkl to resize windows
nnoremap <M-j> :resize -2<CR>
nnoremap <M-k> :resize +2<CR>
nnoremap <M-h> :vertical resize -2<CR>
nnoremap <M-l> :vertical resize +2<CR>

" Easy CAPS
inoremap <C-c> <ESC>viwg~i
nnoremap <C-c> viwg~<Esc>

" TAB in general mode will move to text buffer
nnoremap <TAB> :tabn<CR>
" SHIFT-TAB will go back
nnoremap <S-TAB> :tabp<CR>

" Alternate way to save
nnoremap <C-s> :w<CR>
" Alternate way to quit
nnoremap <C-Q> :wq!<CR>
" Use control-c instead of escape
"nnoremap <C-c> <Esc>

" Rebind F13 (bound to Capslock) to Esc
nnoremap <F13> <Esc>
vnoremap <F13> <Esc>gV
onoremap <F13> <Esc>
cnoremap <F13> <C-C><Esc>
inoremap <F13> <Esc>

" <TAB>: completion.
inoremap <expr><TAB> pumvisible() ? "\<C-n>" : "\<TAB>"

" Better tabbing
vnoremap < <gv
vnoremap > >gv

" Clear highlighting
nnoremap <C-p> :nohl<CR>

" Toggle Word Wrap
nnoremap <M-w> :call AutoWrapToggle()<CR>
function! AutoWrapToggle()
  if &wrap
    echo "Text Wrap Disabled."
    set nowrap 
    nunmap j
    nunmap k
    nunmap $
    nunmap 0
  else
    echo "Text Wrap Enabled."
    set wrap 
    nnoremap j gj
    nnoremap k gk
    nnoremap $ g$
    nnoremap 0 g0
  endif
endfunction

" I forgot what this does
nnoremap <Leader>o o<Esc>^Da
nnoremap <Leader>O O<Esc>^Da

" Abbreviate 'W' and 'Q' to 'w' and 'q' because I keep capitalizing those by mistake.
fun! SetupCommandAlias(from, to)
  exec 'cnoreabbrev <expr> '.a:from
        \ .' ((getcmdtype() is# ":" && getcmdline() is# "'.a:from.'")'
        \ .'? ("'.a:to.'") : ("'.a:from.'"))'
endfun
call SetupCommandAlias("W","w")
call SetupCommandAlias("Wq","wq")
call SetupCommandAlias("Q","q")

" Open terminal in vsplit. Make sure you have ZSH installed. 
nnoremap <Leader>t :vsplit term://zsh<CR>

" =========================
" || (3) Plugin Settings || 
" =========================

" auto-install vim-plug if you don't have it already.
if empty(glob('~/.config/nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall
  "autocmd VimEnter * PlugInstall | source $MYVIMRC
endif

call plug#begin('~/.config/nvim/plugged')
"===[Themes]===
Plug 'itchyny/lightline.vim'    " Better status bar.
Plug 'joshdick/onedark.vim'     " A good dark theme.

"===[File Manager]===
Plug 'lambdalisue/nerdfont.vim' " Nerd font support.
Plug 'lambdalisue/fern.vim'     " File Manager for Vim (Faster than NERDTree). 
Plug 'lambdalisue/fern-renderer-nerdfont.vim' " Use Nerd Font for Fern.
Plug 'lambdalisue/fern-bookmark.vim' "Bookmark function for fern.
Plug 'yuki-yano/fern-preview.vim' " Preview file from Fern buffer.

"===[Visual Aid]===
Plug 'psliwka/vim-smoothie'     " Smooth Scrolling with ctrl+d, ctrl+f, ctrl+u and ctrl+b. 
Plug 'vimoxide/vim-quickscope'  " Makes using f, F, t, and T function quicker.
Plug 'junegunn/goyo.vim'        " Distraction-free writting
Plug 'junegunn/limelight.vim'   " Focus on text by shading unfocused lines.
Plug 'ap/vim-css-color'         " CSS color directly on vim.
Plug 'machakann/vim-highlightedyank' " Highlight yanked/copied text.
Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app && yarn install' }
Plug 'iamcco/markdown.css'      " Github like CSS for markdown preview

"===[Function]===
Plug 'junegunn/vim-easy-align'  " Auto align text.
Plug 'tpope/vim-surround'       " Surround words with a character.
Plug 'tpope/vim-repeat'         " Repeat plugin commands with '.'.
Plug 'farmergreg/vim-lastplace' " Save cursor location on quit.
Plug 'preservim/nerdcommenter'  " Turn a line into a comment.
Plug 'dkarter/bullets.vim'      " Auto bullet point for Markdown.
Plug 'jiangmiao/auto-pairs'     " Autopair Paranthesis, Bracket, etc.

"===[Disabled]===
"Plug 'vimwiki/vimwiki'          " Personal Wiki.
"Plug 'junegunn/vim-journal'     " Syntax highlighting for plain text.
"Plug 'junegunn/fzf', { 'do': { -> fzf#install() } } " Fuzzy finder function.
"Plug 'junegunn/fzf.vim'         " Only needed by dashboard
"Plug 'glepnir/dashboard-nvim'   " Currently disabled for needing lua.
"Plug 'preservim/vim-pencil'     " Useful plugin for writting
"Plug 'tpope/vim-abolish'        " Abbreviate words.
"Plug 'KabbAmine/vCoolor.vim'    " Quick color picker.

call plug#end()

" -----|Theme Setting|-----
" The configuration options should be placed before `colorscheme sonokai`.
colorscheme onedark
let g:lightline={'colorscheme': 'onedark',}

" -----|Fern Setting|-----
let g:loaded_netrw  = 1
let g:loaded_netrwPlugin = 1
let g:loaded_netrwSettings = 1
let g:loaded_netrwFileHandlers = 1
let g:fern_disable_startup_warnings = 1

augroup my-fern-hijack
  autocmd!
  autocmd BufEnter * ++nested call s:hijack_directory()
augroup END

function! s:hijack_directory() abort
  let path = expand('%:p')
  if !isdirectory(path)
    return
  endif
  bwipeout %
  execute printf('Fern %s', fnameescape(path))
endfunction

" Custom settings and mappings.
let g:fern#disable_default_mappings = 1
let g:fern#mapping#bookmark#disable_default_mappings = 0
let g:fern#scheme#bookmark#store#file = "~/.config/nvim/fern-bookmark.json"
let g:fern#default_hidden = 1
let g:fern#renderer= "nerdfont"

noremap <silent> <C-n> :Fern . -drawer -reveal=% -toggle -width=30<CR>
noremap <silent> <C-S-n> :Fern bookmark:/// -drawer -reveal=% -toggle -width=30<CR>

function! FernInit() abort
  nmap <buffer><expr>
        \ <Plug>(fern-my-open-expand-collapse)
        \ fern#smart#leaf(
        \   "\<Plug>(fern-action-open:select)",
        \   "\<Plug>(fern-action-expand)",
        \   "\<Plug>(fern-action-collapse)",
        \ )
  nmap <buffer> <CR> <Plug>(fern-my-open-expand-collapse)
  nmap <buffer> <2-LeftMouse> <Plug>(fern-my-open-expand-collapse)
  nmap <buffer> cc <Plug>(fern-action-new-path)
  nmap <buffer> r <Plug>(fern-action-rename)
  nmap <buffer> R <Plug>(fern-action-remove)
  nmap <buffer> X <Plug>(fern-action-move)
  nmap <buffer> C <Plug>(fern-action-copy)
  nmap <buffer> <C-x> <Plug>(fern-action-clipboard-move)
  nmap <buffer> <C-c> <Plug>(fern-action-clipboard-copy)
  nmap <buffer> <C-p> <Plug>(fern-action-clipboard-paste)
  nmap <buffer> <C-h> <Plug>(fern-action-hidden-toggle)
  nmap <buffer> <C-r> <Plug>(fern-action-reload)
  nmap <buffer> l <Plug>(fern-action-expand)
  nmap <buffer> h <Plug>(fern-action-collapse)
  nmap <buffer> z <Plug>(fern-action-mark:toggle)
  nmap <buffer> t <Plug>(fern-action-open:tabedit)
  nmap <buffer> s <Plug>(fern-action-open:split)
  nmap <buffer> v <Plug>(fern-action-open:vsplit)
  nmap <buffer><nowait> - <Plug>(fern-action-leave)
  nmap <buffer><nowait> = <Plug>(fern-action-enter)
  "Bookmark
  nmap <buffer> B <Plug>(fern-action-save-as-bookmark)
  nmap <buffer> f <Plug>(fern-action-new-leaf)
  nmap <buffer> F <Plug>(fern-action-new-branch)
endfunction

augroup FernGroup
  autocmd!
  autocmd FileType fern call FernInit()
augroup END

function! s:fern_settings() abort
  nmap <silent> <buffer> p     <Plug>(fern-action-preview:toggle)
  nmap <silent> <buffer> P <Plug>(fern-action-preview:auto:toggle)
  nmap <silent> <buffer> <C-d> <Plug>(fern-action-preview:scroll:down:half)
  nmap <silent> <buffer> <C-u> <Plug>(fern-action-preview:scroll:up:half)
endfunction

augroup fern-settings
  autocmd!
  autocmd FileType fern call s:fern_settings()
augroup END

" -----|Vim-Smoothie Setting|-----
nmap <silent> <C-w> <Plug>(SmoothieUpwards)

" -----|Markdown Preview Setting|-----
nmap <M-m> <Plug>MarkdownPreviewToggle
let g:mkdp_browser = 'vimb'

" -----|QuickScope Setting|-----
let g:qs_highlight_on_keys = ['f', 'F', 't', 'T']
nmap <leader>q <plug>(QuickScopeToggle)
xmap <leader>q <plug>(QuickScopeToggle)

" -----|Bullet.vim Setting|-----
let g:bullets_enabled_file_types = [
    \ 'markdown',
    \ 'text',
    \ 'gitcommit',
    \ 'scratch'
    \]
let g:bullets_enable_in_empty_buffers = 1 " default = 1

" -----|Goyo.vim Setting|-----
" Wrapper function to fix the changing background color 
" and allowing quitting vim while Goyo is active.
function! s:goyo_enter()
  let b:quitting = 0
  let b:quitting_bang= 0
  autocmd QuitPre <buffer> let b:quitting = 1
  cabbrev <buffer> q! let b:quitting_bang = 1 <bar> q!
endfunction

function! s:goyo_leave()
  " Restore color
  highlight Normal guifg=#cccccc guibg=#222222 ctermfg=257 ctermbg=256   
  "highlight Normal guifg=#d7d7d7 guibg=#222222 ctermfg=7 ctermbg=234
  " Quit Vim if this is the only remaining buffer
  if b:quitting && len(filter(range(1, bufnr('$')), 'buflisted(v:val)')) == 1
    if b:quitting_bang
      qa!
    else
      qa
    endif
  endif
endfunction

autocmd! User GoyoEnter nested call <SID>goyo_enter()
autocmd! User GoyoLeave nested call <SID>goyo_leave()

" Basic Goyo Setting
let g:goyo_width=100
let g:goyo_height= '90%'
let g:goyo_linenr=1
nnoremap <M-g>  :Goyo<CR> 

" -----|Limelight Setting|-----
let g:limelight_default_coefficient = 0.9
let g:limelight_conceal_ctermfg = 236
nnoremap <M-f> :Limelight!!<CR>

" -----|NERDCommenter Setting|-----
let g:NERDSpaceDelims = 0
map <leader>/ <plug>NERDCommenterToggle

" -----|AutoPairs Setting|-----
let g:AutoPairsShortcutToggle = '<M-a>'

" -----|Vimwiki Setting|-----
"let g:vimwiki_list = [{'path': '~/Documents/Wiki',
                    "\ 'syntax': 'media', 'ext': '.md'}]

" ======================
" || (4) Gui Settings || 
" ======================

" Don't use quotation mark. Use \<space>.
" Set the font in ginit.vim in case this doesn't work.
set guifont=JetBrainsMonoMedium\ Nerd\ Font:h12

" Don't forget to change Goyo color as well 
" If you have color issue with Kitty, change ctermfg and ctermbg to 7 and 234

" Matcha Alice
highlight Normal guifg=#d7d7d7 guibg=#222222 ctermfg=257 ctermbg=256 
"highlight Normal guifg=#d7d7d7 guibg=#222222 ctermfg=7 ctermbg=234

" Matcha Azure
"highlight Normal guifg=#d7d7d7 guibg=#1b1d24 ctermfg=257 ctermbg=256 
"highlight Normal guifg=#d7d7d7 guibg=#1b1d24 ctermfg=7 ctermbg=234

