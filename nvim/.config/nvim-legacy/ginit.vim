" ctrl+6 to return to previous file

if exists(':GuiFont')
    " Use GuiFont! to ignore font errors
    GuiFont JetBrains\ Mono\ Medium:h14
endif

" Enabling Ligatures
if exists(':GuiRenderLigatures')
    GuiRenderLigatures 1
endif

" Disable GUI Tabline
if exists(':GuiTabline')
    GuiTabline 0
endif

" Disable GUI Popupmenu
if exists(':GuiPopupmenu')
    GuiPopupmenu 0
endif

" Set the font in case GUI Neovim doesn't recognize it in init.vim
"GuiFont! FuraCode\ NF:h12
"oooooooo
"
"
"
"
"
"
"
"
"
"
"
"
"
"
