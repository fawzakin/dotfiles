# Neovim Basic Config
This is a Neovim configuration with some useful plugins.
It is nothing more than an adequate text editor for writting documents and editing config files
so it comes with no IDE features configured like LSP, Git, and whatnot. 
If you want those IDE features, please consider using something else like VSCodium, Emacs, or Many Neovim distribution out there.

There are also no important external depedencies so you can use this config to the fullest outside of Linux like Windows. 

You need to have separated ginit.vim to configure gui client like nvim-qt or fvim.

Due to many newer plugins requiring lua configuration, this config may get converted into lua anytime soon.

## Plugin list
#### Themes 
- `itchyny/lightline.vim`: Better status bar.
- `joshdick/onedark.vim`: A good dark theme.
 
#### File Manager
- `lambdalisue/nerdfont.vim`: Nerd font support.
- `lambdalisue/fern.vim`: File Manager for Vim (Faster than NERDTree). 
- `lambdalisue/fern-renderer-nerdfont.vim`: Use Nerd Font for Fern.
- `lambdalisue/fern-bookmark.vim`: Bookmark function for fern.
- `yuki-yano/fern-preview.vim`: Preview file from Fern buffer.
 
#### Visual Aid
- `psliwka/vim-smoothie`: Smooth scrolling with ctrl+d, ctrl+f, ctrl+u and ctrl+b. 
- `vimoxide/vim-quickscope`: Makes using f, F, t, and T function quicker.
- `junegunn/goyo.vim`: Distraction-free writting.
- `junegunn/limelight.vim`: Focus on text by shading unfocused lines.
- `ap/vim-css-color`: CSS color directly on vim.
- `machakann/vim-highlightedyank`: Highlight yanked/copied text.
- `iamcco/markdown-preview.nvim`: Markdown preview through web browser in real time.
 
#### Function 
- `junegunn/vim-easy-align`: Auto align text.
- `tpope/vim-surround`: Surround words with a character.
- `tpope/vim-repeat`: Repeat plugin commands with '.'
- `farmergreg/vim-lastplace`: Save cursor location on quit.
- `preservim/nerdcommenter`: Turn a line into a comment.
- `dkarter/bullets.vim`: Auto bullet point for Markdown.
- `jiangmiao/auto-pairs`: Autopair Paranthesis, Bracket, etc.

#### Disabled
- `vimwiki/vimwiki`: Personal Wiki.
- `junegunn/fzf`: Fuzzy finder function.
- `junegunn/fzf.vim`: Fuzzy finder for some other plugin.
- `glepnir/dashboard-nvim`: Cool startup page for vim.
- `junegunn/vim-journal`: Syntax highlighting for plain text
- `preservim/vim-pencil`: Useful plugin for writting (I only need line wrap feature so I made it myself with alt+w).
- `tpope/vim-abolish`: Abbreviate words.
- `KabbAmine/vCoolor.vim`: Quick color picker.
 
