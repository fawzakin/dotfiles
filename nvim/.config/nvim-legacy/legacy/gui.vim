" gui.vim will only be used for GUI wrapper like Neovim-qt or Neovide.
" Change your color scheme and lightline theme in plugin.vim
"
" Set font for gui (your terminal will take care the font for terminal nvim). 
set guifont=UbuntuMono\ Nerd\ Font:h14

" Set background color to match our global theme.
highlight Normal           guifg=#cccccc     guibg=#222222 ctermfg=257 ctermbg=256   
"highlight Normal           guifg=#ebdbb2     guibg=#222222 ctermfg=15 ctermbg=256   

" Ignore these, colorscheme will take care of everything.
"highlight LineNr           guifg=#7f7f7f     guibg=#222222 ctermfg=8 
"highlight CursorLine                                       ctermbg=8 cterm=none
"highlight CursorLineNr     guifg=#e5e5e5     guibg=none    ctermfg=7
"highlight VertSplit        guifg=#7f7f7f     guibg=#7f7f7f
"highlight Statement        guifg=#ffff00     guibg=none       
"highlight Directory        guifg=#2a2aff     guibg=none       
"highlight StatusLine       guifg=#e5e5e5     guibg=#7f7f7f 
"highlight StatusLineNC     guifg=#e5e5e5     guibg=#7f7f7f  
"highlight NERDTreeClosable guifg=#00cd00     guibg=none    ctermfg=2
"highlight NERDTreeOpenable guifg=#e5e5e5     guibg=none    ctermfg=7 
"highlight Comment          guifg=#00cdcd     guibg=none    ctermfg=6 gui=italic cterm=italic 
"highlight Constant         guifg=#00ffff     guibg=none       
"highlight Special          guifg=#2a2aff     guibg=none    ctermfg=4     
"highlight Identifier       guifg=#cdcd00     guibg=none    ctermfg=3   
"highlight PreProc          guifg=#cd00cd     guibg=none    ctermfg=5   
"highlight String           guifg=#7ec0ff     guibg=none    ctermfg=12   
"highlight Number           guifg=#ff0000     guibg=none    ctermfg=9     
"highlight Function         guifg=#cd0000     guibg=none       
"highlight Visual           guifg=#e5e5e5     guibg=#7f7f7f    

" Misc. settings
"set guioption-=m    " remove menu bar
"set guioption-=T    " remove toolbar
"set guioption-=r    " remove right-hand scrollbar
"set guioption-=L    " remove left-hand scrollbar
