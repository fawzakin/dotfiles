set nocompatible " Reset options when re-sourcing vim settings
syntax enable " Enable syntax highlighting
set hidden " Multiple buffers
set nowrap " No text wraping
set noshowmode " Disable duplicated mode text if you use airline
set wildmenu " Better cli completion
set showcmd " Show partial commands
set hlsearch " Highlight search. Use Ctrl+P to disable it
set ignorecase smartcase " Case insensitive search, except when using capital letters
set backspace=indent,eol,start " Allow backspacing over autoindent, line breaks and start of insert action
set autoindent " keep the same indent as the line you're currently on.
set smartindent " smart indenting
set nostartofline " Stop certain movements from always going to the first character of a line.
set ruler " Show cursor potition all the time
set laststatus=2 " Always display the status line, even if only one window is displayed
set confirm " Set dialog if you want to save changed files instead of failure
set visualbell " Visual bell instead of beep
set t_vb=
set mouse=a " Enable mouse
set cmdheight=1 " Command window height
set number relativenumber " Display line number
set notimeout ttimeout ttimeoutlen=200 " Quickly time out on keycodes, but never time out on mappings
set pastetoggle=<F11> " Use <F11> to toggle between 'paste' and 'nopaste'
set shiftwidth=4 " Use 4 spaces instead of tab
set softtabstop=4
set expandtab
set encoding=utf-8 " Display and write encoding
set fileencoding=utf-8  
set formatoptions-=cro " Disable comment when making new lines
set iskeyword+=- " Make dash a word
set splitright " Attach new buffer window to the right and bellow automatically
set splitbelow
set showtabline=1 " Always show tabs
set t_Co=256 " Support 256 colors
set clipboard+=unnamedplus " Make copy pasting in and out vim easier
"set cursorline "Highlight current line
au! BufWritePost $MYVIMRC source % " Autosource when writing to init.vim
if has('filetype') " Filetype spesific thing
  filetype indent plugin on
endif


