" map explanations:
" map: Map a key.
" noremap: Map a key non-recursively. All Mode
" nnoremap: Map a key non-recursively. Normal mode.
" inoremap: Map a key non-recursively. Insert mode.
" vnoremap: Map a key non-recursively. Visual mode.

" Change the leader key to comma. Useful for custom commands.
let mapleader = ","

" Better nav for omnicomplete
inoremap <expr> <c-j> ("\<C-n>")
inoremap <expr> <c-k> ("\<C-p>")

" Better window navigation
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" Use ctrl + shift + hjkl to resize windows
nnoremap <M-j>    :resize -2<CR>
nnoremap <M-k>    :resize +2<CR>
nnoremap <M-h>    :vertical resize -2<CR>
nnoremap <M-l>    :vertical resize +2<CR>

" Easy CAPS
inoremap <C-c> <ESC>viwg~i
nnoremap <C-c> viwg~<Esc>

" TAB in general mode will move to text buffer
nnoremap <TAB> :tabn<CR>
" SHIFT-TAB will go back
nnoremap <S-TAB> :tabp<CR>

" Alternate way to save
nnoremap <C-s> :w<CR>
" Alternate way to quit
nnoremap <C-Q> :wq!<CR>
" Use control-c instead of escape
"nnoremap <C-c> <Esc>

" Rebind F13 (bound to Capslock) to Esc
nnoremap <F13> <Esc>
vnoremap <F13> <Esc>gV
onoremap <F13> <Esc>
cnoremap <F13> <C-C><Esc>
inoremap <F13> <Esc>

" <TAB>: completion.
inoremap <expr><TAB> pumvisible() ? "\<C-n>" : "\<TAB>"

" Better tabbing
vnoremap < <gv
vnoremap > >gv

" Clear highlighting
nnoremap <C-P> :nohl<CR><C-P>

" Toggle Word Wrap
nnoremap <M-w> :call AutoWrapToggle()<CR>
function! AutoWrapToggle()
  if &wrap
    set nowrap 
    nunmap j
    nunmap k
    nunmap $
    nunmap 0
  else
    set wrap 
    nnoremap j gj
    nnoremap k gk
    nnoremap $ g$
    nnoremap 0 g0
  endif
endfunction

" I forgot what this does
nnoremap <Leader>o o<Esc>^Da
nnoremap <Leader>O O<Esc>^Da
