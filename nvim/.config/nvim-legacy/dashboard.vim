"This is my legacy config for dashboard before the new update removes
"vimscript config. Everything is going to be replaced with Lua.

" -----|Colorizer Setting|-----
if has('termguicolors')
    set termguicolors
endif

lua require'colorizer'.setup()

" -----|FZF Setting|-----
nnoremap <C-/> :Lines<CRegularR>

" -----|Dashboard Setting|-----
let g:dashboard_custom_header =<< trim END
VVVVVVVV           VVVVVVVV IIIIIIIIII MMMMMMMMMMM         MMMMMMMMMMM
 V:::::V           V:::::V  II::::::II M::::::::::M       M::::::::::M
  V:::::V         V:::::V     I::::I   M:::::::::::M     M:::::::::::M
   V:::::V       V:::::V      I::::I   M:::::::M::::M   M::::M:::::::M
    V:::::V     V:::::V       I::::I   M::::::M M::::M M::::M M::::::M
     V:::::V   V:::::V        I::::I   M::::::M  M::::M::::M  M::::::M
      V:::::V V:::::V         I::::I   M::::::M   M:::::::M   M::::::M
       V:::::V:::::V          I::::I   M::::::M    M:::::M    M::::::M
        V:::::::::V           I::::I   M::::::M     MMMMM     M::::::M
         V:::::::V          II::::::II M::::::M               M::::::M
          VVVVVVV           IIIIIIIIII MMMMMMMM               MMMMMMMM
END
let g:dashboard_default_executive ='fzf' " Use fzf as pop-up window
let g:dashboard_custom_footer = ['Start Menu']

nmap <Leader>ss :<C-u>SessionSave<CR>
nmap <Leader>sl :<C-u>SessionLoad<CR>
nnoremap <silent> <Leader>db :Dashboard<CR>
nnoremap <silent> <Leader>rf :DashboardFindHistory<CR>
nnoremap <silent> <Leader>nf :DashboardNewFile<CR>
nnoremap <silent> <Leader>ff :DashboardFindFile<CR>
nnoremap <silent> <Leader>fw :DashboardFindWord<CR>
nnoremap <silent> <Leader>bb :Fern bookmark:///<CR>
nnoremap <silent> <Leader>ec :edit ~/.config/nvim/init.vim<CR>
nnoremap <silent> <Leader>ed :Fern ~/.config<CR>
"I don't really need this function
"nnoremap <silent> <Leader>tc :DashboardChangeColorscheme<CR>
"nnoremap <silent> <Leader>fb :DashboardJumpMark<CR>

let g:dashboard_custom_section={
    \ '1_vim_wiki': {
        \ 'description': ['ﴬ Open Vim Wiki           <SPC>ww'],
        \ 'command': 'VimwikiIndex' },
    \ '2_recent_file': {
        \ 'description': [' Recent File             <SPC>rf'],
        \ 'command': 'DashboardFindHistory' }, 
    \ '3_new_file': {
        \ 'description': [' New File                <SPC>nf'],
        \ 'command': 'DashboardNewFile' },
    \ '4_find_file': {
        \ 'description': [' Find File               <SPC>ff'],
        \ 'command': 'DashboardFindFile' },
    \ '5_find_word': {
        \ 'description': [' Find Word               <SPC>fw'],
        \ 'command': 'DashboardFindWord' },
    \ '6_book_marks': {
        \ 'description': [' Bookmarks               <SPC>bb'],
        \ 'command': 'Fern bookmark:///' },
    \ '7_last_session': {
        \ 'description': [' Last Session            <SPC>sl'],
        \ 'command': 'SessionLoad' },
    \ '8_edit_config': {
        \'description': [' Edit Dotfiles   <SPC>ec/<SPC>ed'],
        \ 'command': 'Fern ~/.config' }
    \ }
