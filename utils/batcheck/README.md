# batcheck
A simple program to remind me to charge my Laptop when the battery is 10% or lower. That's all.

### Installation
1. Stow this folder/package.
2. run `sudo make install` for installing the `batcheck` script
3. Run `systemctl --user start --now batcheck.timer` as user
4. The script should check the battery every 10 minutes
