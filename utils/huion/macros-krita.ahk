; Keys are sorted by row
; From left to right

; Row 1
NumpadMult::
Send, e
return
 
; Row 2
Numpad7::
Send, {Ctrl down}t{Ctrl up}
return

Numpad8::
Send, {Ctrl down}r{Ctrl up}
return

Numpad9::
Send, {Ctrl down}k{Ctrl up}
return

; Row 3
Numpad4::
Send, b
return

Numpad5::
Send, t
return

Numpad6::
Send, p
return

; Row 4
Numpad1::
Send, {Ctrl down}x{Ctrl up}
return

Numpad2::
Send, {Ctrl down}c{Ctrl up}
return

Numpad3::
Send, {Ctrl down}v{Ctrl up}
return

; Row 5
Numpad0::
Send, {Ctrl down}z{Ctrl up}
return

NumpadDot::
Send, {Ctrl down}{Shift down}z{Shift up}{Ctrl up}
return

NumpadEnter::
Send, {Ctrl down}s{Ctrl up}
return
