![H640P](https://www.huion.com/statics/hw/site/img/h640p-unleash-pic.jpg)
# Huion Tablet Config
These are my scripts and configurations for my Huion H640P tablet for drawing purpose. It also comes with scripts and printable labels to turn an external numpad into a macropad binded for Krita's default keybinding. [See the labels down below](#labels).

### Installation
1. Stow this folder/package.
2. run `sudo make install` for installing the main `huionset` scripts (the rest should stay in ~/.local/bin) and the udev rules.
3. Run `systemctl --user start --now huion.service` as user. Everything should be configured once the tablet is inserted.
4. Add your user to `input` group with `sudo usermod -aG input $USER`.
5. Open Krita and add keybinding to some functions [as written here](#macros-krita).

# List of scripts
### huionset
Configuration for Huion H640P Tablet.  
My button mapping (Right handed layout, Buttons on the left side):
- Shift
- Control
- Control + Shift + A (deselect all)
- R (reset rotation, requires to be binded in Krita first, see [macross-krita](#macros-krita))
- M (mirror view)
- Alt + Tab (Switch to last opened windows/workspace on Windows/dwm)

### huionside
Switch monitor side for Huion Tablet with the use of a portable second monitor with the resolution of 1366x768 placed on the right side of the main screen.

If you're running [my build of dwm](https://gitlab.com/fawzakin/dwm) and your numpad has a multimedia row, it can be toggled using the "Mail" media button.

### kp-event
Return the event device for an external numpad. Helper script for the next script. Doesn't do anything on its own.

### macros-krita
Python script to turn an external numpad into a macropad for krita. Requires python evdev and xdotool.  
You can see my macros numpad layout [down below](#numpad-macro-binding).

All macros are set to Krita's default keybinding. However, there are two keybinding that haven't been set by Krita by default. Those are the following:
1. Reset Canvas Rotation: set to "r"
2. Freehand Selection Tool: set to "ctrl+k"

There is also `macro-krita.ahk` which is AutoHotKeys version of this script for Windows.

### macros-krita-alt
Alternate version of above that requires python-pynput instead of xdotool.

### macros-toggle
Start or stop a running `macros-krita` instance. 

If you're running [my build of dwm](https://gitlab.com/fawzakin/dwm) and your numpad has a multimedia row, it can be toggled using the "HomePage" media button.

# Labels
The following are labels that I printed onto a A4 sticker paper and applied to my numpad and Huion tablet. All square labels are 7x7 mm while huion hotkey labels are 11x7 mm. You can download them in your preferred format here:  
[PDF](buttons/Hotkey_Labels.pdf) | [PNG](buttons/Hotkey_Labels.png) | [SVG](buttons/Hotkey_Labels.svg)

### Numpad Macro Binding 
The following table is based on the general layout of most numpads. Note that some numpad may put the backspace key on different position and some may have an extra row that contains some multimedia keys (which wouldn't be shown here).  The bindings are taken from `macros-krita` python script or `macros-krita.ahk` AutoHotkey script.

Hover your mouse to each label icon to see its function in Krita.
|**Row/Column**|**Column 1**|**Column 2**|**Column 3**|**Column 4**|
|---|---|---|---|---|
|**Row 1**|Numlock| ![2](buttons/KPSlash-Switch.png "Switch to Previous Preset") | ![3](buttons/KPAsterisk-Eraser.png "Eraser Mode") | ![4](buttons/KPBackspace-SaveInc.png "Save Incremental") |
|**Original**||/|\*|Backspace|
|**Binding**||Unchanged|E|Ctrl+Alt+S|
|**Row 2**| ![1](buttons/KP7-Transform.png "Transform Selection") | ![2](buttons/KP8-Select.png "Rectangular Selection") | ![3](buttons/KP9-FreeSelect.png "Freehand Selection") | ![4](buttons/KPMinus-Out.png "Zoom Out") |
|**Original**|7|8|9|-|
|**Binding**|Ctrl+T|Ctrl+R|Ctrl+K|Unchanged|
|**Row 3**| ![1](buttons/KP4-Brush.png "Brush") | ![2](buttons/KP5-Move.png "Move") | ![3](buttons/KP6-Picker.png "Color Picker") | ![4](buttons/KPPlus-In.png "Zoom In") |
|**Original**|4|5|6|+|
|**Binding**|B|T|P|Unchanged|
|**Row 4**| ![1](buttons/KP1-Cut.png "Cut") | ![2](buttons/KP2-Copy.png "Copy") | ![3](buttons/KP3-Paste.png "Paste") | ![4](buttons/KPEnter-Save.png "Save") |
|**Original**|1|2|3|Enter|
|**Binding**|Ctrl+X|Ctrl+C|Ctrl+V|Ctrl+S|
|**Row 5**| ![1](buttons/KP0-Undo.png "Undo") | ![2](buttons/KP000-Undo3x.png "Undo Three Times") | ![3](buttons/KPDot-Redo.png "Redo") | ![4](buttons/KPEnter-Save.png "Save") |
|**Original**|0|000|.|Enter|
|**Binding**|Ctrl+Z|Ctrl+Z x3|CtrSh+Z|Ctrl+S|

\*CtrSh = Ctrl+Shift

### Huion Hot Keys
The following bindings are configured using `huionset` script on Linux or [Huion's official driver on Windows/MacOS](https://www.huion.com/index.php?m=content&c=index&a=lists&catid=16&down_title2=H640P) (You have to manually set them yourself).

Hover your mouse to each label icon to see its function.

|Button|Label|Binding|
|---|---|---|
|**Button 1**|![key](buttons/Huion1-Shift.png "Shift")|Shift|
|**Button 2 •**|![key](buttons/Huion2-Ctrl.png "Control")|Ctrl|
|**Button 3**|![key](buttons/Huion3-Desel.png "Deselect")|CtrSh+A|
|○|||
|**Button 4**|![key](buttons/Huion4-ResRot.png "Reset Rotation")|R|
|**Button 5 •**|![key](buttons/Huion5-Mir.png "Mirror")|M|
|**Button 6**|![key](buttons/Huion6-Tab.png "Switch Window/Workspace")|Alt+Tab|

\*CtrSh = Ctrl+Shift
