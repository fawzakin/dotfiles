# The New and Improved Dotfiles + Installer
A collection of my personal Linux settings. Now using `stow` for easier dotfile management and many apps are group together for cleaner list.  

See the [Stow Packages](#stow-packages) below for more information about each package.

This repository also includes installer to install every necessary packages.

# Installing
Make sure you have `git` and `stow` installed in your system.

### Instruction:
Clone this repo somewhere in your home directory (e.g. `~/.dotfiles`).
```
git clone https://gitlab.com/fawzakin/dotfiles.git ~/.dotfiles
```
`cd` into the directory and run stow like this.
```
stow --no-folding -t ~ */
```
If you encounter a conflict error (or a weird regex error), run this command or `stow-install.sh`.  
```
for dir in */; do stow --no-folding -t ~ "$dir" 2> /dev/null || echo "$dir" >> ~/.stow-conflict; done
```
It will also output a list of conflicting directories to `~/.stow-conflict`.  
Delete or Move any conflicting folders reported in the list and then run this again.

If you just want a specified config, run stow like this.
```
stow --no-folding -t ~ zsh
```

# Stow Packages
This repository is divided into packages and each package groups software based of its category. The reason behind this is to allow you to choose which configuration you just want to apply. Grouping them into categories also make this repository look so much cleaner and organized.

The following are the packages and what kind of config I put inside them:
- bin
    - For some scripts I made for myself.
    - See [bin/README](bin/README.md)
- hyprland
    - Anything that's required for Hyprland to function.
    - Contains config for Hyprland, Waybar, many other Hypr tools, and bunch of Hypr scripts.
- media
    - Media consumption (this includes PDF viewing).
    - Contains config for mpd, mpDris2, mpv, ncmpcpp, and zathura.
- misc
    - Small stuff not worth for its own stow package.
    - Contains config for btop, dunst, flameshot, gammastep, godot (4.3, 4.2, 3.5), lf, rofi, swayimg, vimb, and specialized fonts.
- nvim
    - Neovim configuration based of [kickstart.nvim](https://github.com/nvim-lua/kickstart.nvim) project.
    - Also includes other configuration for neovim GUIs such as neovide, nvim-qt, and nvim-vscode.
    - nvim-legacy is the old pre-lua configuration.
- old_apps
    - Applications I don't use anymore but I'd still like to keep their config.
    - Contains config for jgmenu and kitty.
    - Please don't stow this if you plan to use dwm. Use the one provided by the new suckless repo!
- pictures
    - Contains default wallpaper and lockscreen like you see in [Desktop Overview](#desktop-overview).
- rofi
    - Contains config for rofi as well as scripts that mainly uses rofi.
    - See [rofi/README](rofi/README.md)
- terminals
    - The terminals I use.
    - Contains config for alacritty and foot.
- themes
    - Contains config for gtk and qt themes.
    - Doesn't actually contain any themes. [Please install Dracula separately](https://draculatheme.com).
- utils
    - This package can't be stowed.
    - See [batcheck/README](utils/huion/README) and [huion/README](utils/huion/README) for each installation guide.
- zsh
    - The main login shell I use.
    - Contains config for zsh and starship.
 
# Desktop Overview

### dwm
![Screenshot of dwm](dwm.png)
*NOTE: The bar is from dwm itself with status2d patch, not from polybar.*

This is my current window manager for daily use. The wallpaper comes with this dotfile repo.

Check the config for dwm and other suckless tools [here](https://gitlab.com/fawzakin/sl).

### Hyprland
![Screenshot of hyprland](hyprland.png)

This is my Wayland window manager with Waybar as the bar.

Check the config for both Hyprland and Waybar [here](hyprland/.config).

### Themes
Default theme for all programs is [Dracula Theme](https://draculatheme.com/). I am abandoning my old Matcha theme in favor of Dracula.

# License and Credit
MIT License for all files unless specified otherwise by `LICENSE` inside some directories if any.

lf previewer scripts by [slavistan](https://github.com/slavistan/lf-gadgets/tree/master/lf-ueberzug). Modified by me to show music album art.

Before you complain, the repo's icon is only a ret dot (from the term "dotfiles") in front of dark background.  
It looks like the face of [this robot](https://wanderoveryonder.fandom.com/wiki/Beep_Boop) if you ask me.
